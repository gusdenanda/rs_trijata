<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenandatanganSwab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penandatangan_swab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ps_nama');
            $table->string('ps_pangkat');
            $table->string('ps_nrp');
            $table->integer('ps_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penandatangan_swab');
    }
}
