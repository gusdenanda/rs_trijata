<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHasilSwab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_swab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hs_nama');
            $table->string('hs_nik');
            $table->date('hs_tgl_lahir');
            $table->integer('hs_usia');
            $table->integer('hs_jk');
            $table->string('hs_no_hp');
            $table->text('hs_alamat');
            $table->string('hs_faskes');
            $table->integer('hs_sampel_ke');
            $table->string('hs_jenis_spesimen');
            $table->string('hs_nomor_spesimen');
            $table->date('hs_tgl_kirim_spesimen');
            $table->date('hs_tgl_terima_spesimen');
            $table->date('hs_tgl_periksa_spesimen');
            $table->string('hs_hasil');
            $table->string('hs_kriteria');
            $table->string('hs_satker');
            $table->integer('skp_id');
            $table->string('hs_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_swab');
    }
}
