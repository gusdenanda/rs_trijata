<!DOCTYPE html>
<html>
<head>
    <title>Hasil SWAB {{$hs_nama}}</title>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 60px;
            header: page-header;
	        footer: page-footer;
        }
        header {
            position: fixed;
            top: -100px;
            left: -65px;
            right: -65px;
            height: 70px;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed; 
            bottom: -60px; 
            left: -65px; 
            right: -65px;
            height: 90px; 

            /** Extra personal styles **/
            text-align: center;
            line-height: 35px;
        }
        .tableborder {
            border-left: 0.01em solid #000;
            border-right: 0;
            border-top: 0.01em solid #000;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table {
            border-collapse: collapse;
        }
        .tabletdborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0.01em solid #000;
            border-bottom: 0.01em solid #000;
            padding: 5px;
        }
        
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_atas.png") }}" alt="" style="width: 120%; margin:-40px -65px 0px -65px; ;">
    </htmlpageheader>
    <main>
        <br><br>
        <p style="text-align: center; font-size:20px; line-height: 22px; font-weight:bold;"><u>HASIL PEMERIKSAAN COVID-19</u> <br><i style="font-size:16px;">(EXAMINATION RESULT FOR COVID-19)</i></p>
        <br>
        <table style="width:100%">
            <tr>
                <td>
                    <table style="width:100%; border:none">
                        <tr>
                            <td style="width:33%; border:none;" valign="top"><b>SAMPEL ID </b><br> <i>SAMPLE ID</i></td>
                            <td style="width:3%; border:none;"  valign="top">:</td>
                            <td style="border:none;"  valign="top"><b>{{$hs_nomor_spesimen_antigen}}</b></td>
                        </tr>
                        <tr>
                            <td  valign="top"><b>TANGGAL PEMERIKSAAN</b><br><i>CHECK DATE</i></td>
                            <td  valign="top">:</td>
                            <td  valign="top"><b>{{date("d-m-Y",strtotime($hs_tgl_periksa_spesimen))}} 
                            @if ($hs_jam_periksa_spesimen!="00:00:00" && $hs_jam_periksa_spesimen!="")
                                {{date("H:i",strtotime($hs_jam_periksa_spesimen))}} WITA
                            @endif
                            </b></td>
                        </tr>
                        <tr>
                            <td  valign="top"><b>NAMA</b><br><i>NAME</i></td>
                            <td  valign="top">:</td>
                            <td  valign="top"><b>{{$hs_nama}}</b></td>
                        </tr>
                        <tr>
                            <td  valign="top"><b>TANGGAL LAHIR</b> <br><i>DATE OF BIRTH </i></td>
                            <td  valign="top">:</td>
                            <td  valign="top"><b>{{date("d-m-Y",strtotime($hs_tgl_lahir))}}</b></td>
                        </tr>
                        <tr>
                            <td  valign="top"><b>UMUR/KELAMIN</b><br><i>AGE/GENDER </i></td>
                            <td  valign="top">:</td>
                            <td  valign="top"><b>
                                {{$hs_usia}} Thn / {{ucwords($hs_jk)}} 
                                @if (strtolower($hs_jk)=="perempuan")
                                    <i>(Female)</i>
                                @else
                                    <i>(Male)</i>
                                @endif
                                </b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <table style="width:100%; font-size:18px;" class="tableborder">
            <tr>
                <td style="width: 40%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">PARAMETER <br><i style="font-size:15px;">PARAMETER</i></td>
                <td style="width: 30%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">HASIL <br><i style="font-size:15px;">RESULT</i></td>
                <td style="text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">NILAI NORMAL <br><i style="font-size:15px;">NORMAL RANGE</i></td>
            </tr>
            <tr>
                <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Covid-19 Antigen Rapid Test</td>
                @if (strtolower($hs_hasil)=="positif")
                    <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">{{strtoupper($hs_hasil)}} <br><i>POSITIVE</i></td>
                @else
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{strtoupper($hs_hasil)}} <br><i>NEGATIVE</i></td>
                @endif
                <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">NEGATIF <br><i>NEGATIVE</i></td>
            </tr>
        </table>
        <br>
        <p style="font-size:14px;"><b><u>CATATAN</u></b> <i>(NOTES)</i></p>
        <ul style="font-size:12px;">
            <li>Hasil Negatif tidak memungkinkan kemungkinan terinfeksi SARS-CoV-2 sehingga masih berisiko menularkan ke orang lain, disarankan tes ulang atau tes konfirmasi dengan NAAT, bila probilitas pretest relatif tinggi terutama bila pasien bergejala atau diketahui kontak orang yang terkonfirmasi Covid-19.
                <i>Negative results do not rule out the possibility of being infected with SARS-CoV-2 so that there is still a risk of transmitting it to others, it is recommended to retest or confirmatory tests with NAAT, if the pretest probability is relatively high, especially if the patient is symptomatic or is known to have contact with a confirmed Covid-19 person.</i>
            </li>
            <li>
                Hasil Negatif dapat terjadi pada kondisi kuantitas antigen pada spesimen di bawah tingkat deteksi alat.
                <i>Negative results can occur when the quantity of antigen in the specimen is below the detection level of the instrument.</i>
            </li>
            <li>
                Diperiksa Oleh  :
                @if ($hs_diperiksa_oleh!="")
                    {{$hs_diperiksa_oleh}}
                @else
                    Analis
                @endif
                <br>
                <i>Checked by </i>
            </li>
            <li>
                Diverifikasi oleh :
                @if ($hs_diverifikasi_oleh!="")
                    {{$hs_diverifikasi_oleh}}
                @else
                    dr. Herry Herlambang Sp.PK
                @endif
                <br>
                <i>Verificated by </i>
            </li>
        </ul>
        <br>
        <table style="border:0; width:100%">
            <tr>
                <td style="text-align: right; width:14%"><img src="data:image/svg;base64, {!! $qrcode !!}"></td>
                <td style="font-size: 12px; width:80%">Silahkan scan QR-Code di samping untuk melakukan validasi terhadap dokumen hasil Swab Test ini. <br> 
                <i>Please scan the QR-Code to validate the results of this Swab Test.</i></td>
                <td></td>
            </tr>
        </table>
        
    </main>
    <htmlpagefooter name="page-footer">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_bawah.png") }}" alt="" style="width: 100%;" style="width: 120%; margin:0px -65px -40px -65px; ;">
    </htmlpagefooter>
    
    
</body>
</html>