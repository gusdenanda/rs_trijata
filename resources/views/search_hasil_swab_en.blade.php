@extends('layout.guest')

@section('title', 'Hasil Swab Test')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/page-auth.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
@endsection

@section('content')
	<!-- Error page-->
    <div class="auth-wrapper auth-v2">
        <div class="auth-inner row m-0">
            <a class="brand-logo" href="javascript:void(0);">
                <img src="{{ asset('app-assets/images/ico/logo-rs.png') }}" class="img-fluid" alt="Brand logo" style="width: 50px; height:50px;">
                <h2 class="brand-text text-success ml-1" style="padding-top:10px; ">E-Swab Bhayangkara Denpasar Hospital</h2>
            </a>
            <div class="d-none d-lg-flex col-lg-6 align-items-center p-5">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="{{asset('app-assets/images/pages/reset-password-v2.svg')}}" alt="Login" /></div>
            </div>
            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5 ">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto" style="margin-top:110px;">
                    <img src="{{ asset('app-assets/images/ico/logo-rstrijata.png') }}" class="img-fluid"> <br><br>
                    <p class="card-text mb-2 text-center">Please fill your <b>Birthdate</b> and <b>Check date</b> to search <b>SWAB Test Results</b></p>
                    <form class="auth-login-form mt-2" action="{{ route('swabtest-result.search') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="form-label" for="login-email">Birthdate</label>
                            <input class="form-control flatpickr-basic" id="tanggal_lahir" type="text" name="tanggal_lahir" placeholder="Birthdate"  autofocus="" tabindex="1" />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="login-email">Check Date</label>
                            <input class="form-control flatpickr-basic" id="tanggal_periksa" type="text" name="tanggal_periksa" placeholder="Check Date"  autofocus="" tabindex="2" />
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" tabindex="4">Search...</button>
                    </form>
                    <div class="divider my-2">
                        <div class="divider-text">or</div>
                    </div>
                    <p class="text-center mt-2">
                        <a href="/login"><b>Login</b></a>
                    </p>
                    <a href={{url('/hasil-swabtest')}}><p class="card-text mb-2 text-center">Change To Indonesian Version <img src="{{ asset('app-assets/images/ico/id.png') }}"  class="img-fluid"></p></a>
                    
                </div>
            </div>
            <!-- /Login-->
        </div>
    </div>
<!-- / Error page-->
@endsection
@section('js')
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>

	<script>
		// Modal
		$(document).ready(function() {
			$('.flatpickr-basic').flatpickr();
        });
	</script>
@endsection
