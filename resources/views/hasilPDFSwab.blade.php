<!DOCTYPE html>
<html>
<head>
    <title>Hasil SWAB {{$hs_nama}}</title>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 60px;
            header: page-header;
	        footer: page-footer;
        }
        header {
            position: fixed;
            top: -100px;
            left: -65px;
            right: -65px;
            height: 70px;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed; 
            bottom: -60px; 
            left: -65px; 
            right: -65px;
            height: 90px; 

            /** Extra personal styles **/
            text-align: center;
            line-height: 35px;
        }
        .tableborder {
            border-left: 0.01em solid #000;
            border-right: 0;
            border-top: 0.01em solid #000;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table {
            border-collapse: collapse;
        }
        .tabletdborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0.01em solid #000;
            border-bottom: 0.01em solid #000;
            padding: 5px;
        }
        
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_atas.png") }}" alt="" style="width: 120%; margin:-40px -65px 0px -65px; ;">
    </htmlpageheader>
    <main>
        <br><br><br>
        <p style="text-align: center; font-size:20px; line-height: 22px; font-weight:bold;"><u>HASIL PEMERIKSAAN COVID-19</u> <br><i style="font-size:16px;">(EXAMINATION RESULT FOR COVID-19)</i></p>
        <br>
        <table style="width:100%;" class="tableborder">
            <tr>
                <td style="width: 45%; line-height: 28px;" valign="center" class="tabletdborder" style="border-left: 0.01em solid #000;;
                border-right: 0.01em solid #000;
                border-top: 0.01em solid #000;
                border-bottom: 0.01em solid #000;
                padding: 5px;">
                    <table style="width:100%; border:1px; font-size:12px;" >
                        <tr>
                            <td tyle="width:100%;"><b>NIK/NO PASSPORT : {{$hs_nik}} </b><br><i>IDENTITY NUMBER </i></td>
                        </tr>
                        <tr>
                            <td ><b>NAMA : {{$hs_nama}} </b><br><i>NAME </i></td>
                        </tr>
                        <tr>
                            <td ><b>TANGGAL LAHIR : {{date("d-m-Y",strtotime($hs_tgl_lahir))}} </b> <br><i>DATE OF BIRTH  </i></td>
                        </tr>
                        <tr>
                            <td><b>UMUR/KELAMIN : {{$hs_usia}} Thn / {{ucwords($hs_jk)}} </b> <br><i>AGE/GENDER </i></td>
                        </tr>
                        <tr>
                            <td><b>ALAMAT: {{$hs_alamat}}</b> <br><i>ADDRESS </i></td>
                        </tr>
                        @if ($hs_ct_value!="")
                        <tr>
                            <td>
                                <b>CT VALUE : </b>
                                    @if ($hs_ct_value > $ct_value)
                                        Tidak terdeteksi (Cut off ≤ {{$ct_value}}) <br> <i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Not detected(Cut off ≤ {{$ct_value}}) </i>
                                    @else
                                        {{$hs_ct_value}} (Cut off ≤ {{$ct_value}})
                                    @endif
                           </td>
                        </tr>
                        @endif
                    </table>
                </td>
                <td style="width: 55%; line-height: 28px;" valign="center" class="tabletdborder">
                    <table style="width:100%; border:1px; font-size:12px;">
                        <tr>
                            <td style="width: 100%"><b>No Telepon : {{$hs_no_hp}}</b> <br><i>PHONE </i></td>
                        </tr>
                        <tr>
                            <td><b>KODE SPESIMEN : {{$hs_nomor_spesimen}}</b> <br><i>SPESIMEN CODE </i></td>
                        </tr>
                        <tr>
                            <td><b>TGL PENGAMBILAN SPESIMEN : {{date("d-m-Y",strtotime($hs_tgl_kirim_spesimen))}} 
                            @if ($hs_jam_kirim_spesimen!='00:00:00' && $hs_jam_kirim_spesimen!='' && $hs_jam_kirim_spesimen!=NULL)
                                {{date("H:i",strtotime($hs_jam_kirim_spesimen))}} WITA 
                            @endif
                            </b> <br><i>SWAB DATE</i></td>
                        </tr>
                        <tr>
                            <td><b>TGL SPESIMEN SELESAI : {{date("d-m-Y",strtotime($hs_tgl_periksa_spesimen))}} 
                            @if ($hs_jam_periksa_spesimen!="00:00:00" && $hs_jam_periksa_spesimen!="")
                                {{date("H:i",strtotime($hs_jam_periksa_spesimen))}} WITA
                            @endif
                            </b> <br><i>RESULT RELEASE DATE</i></td>
                        </tr>
                        <tr>
                            <td><b>SPESIMEN KE : {{$hs_sampel_ke}}</b> <br><i>SPECIMEN TO</i></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br><br>
        <table style="width:100%; font-size:18px;" class="tableborder">
            <tr>
                <td style="width: 35%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">JENIS PEMERIKSAAN<br><i style="font-size:15px;">INVESTIGATION</i></td>
                <td style="width: 20%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">HASIL <br><i style="font-size:15px;">RESULT</i></td>
                <td style="width: 25%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">NILAI NORMAL<br><i style="font-size:15px;">NORMAL RANGE</i></td>
                <td style="width: 20%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">METODE<br><i style="font-size:15px;">METHOD</i></td>
            </tr>
            <tr>
                <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">SARS-CoV-2</td>
                @if (strtolower($hs_hasil)=="positif")
                    <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">{{strtoupper($hs_hasil)}} <br><i style="font-size: 14px;">POSITIVE</i></td>
                @else
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{strtoupper($hs_hasil)}} <br><i style="font-size: 14px;">NEGATIVE</i></td>
                @endif
                <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">NEGATIF <br> <i style="font-size: 14px;">NEGATIVE</i></td>
                <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">RT-PCR</td>
            </tr>
        </table>
        <br>
        <p style="font-size:14px;"><b><u>CATATAN</u></b> <i>(NOTES)</i></p>
        <ul style="font-size:12px;">
            <li>Pemeriksaan dilakukan dengan metode Realtime PCR dan hanya menggambarkan Kondisi saat   spesimen diambil.<br>
                <i>The examination is carried out using the Realtime PCR method and only describes the condition when the specimen was taken.</i>
            </li>
            <li>
                Ijin operasional Laboratorium PCR RS Bhayangkara Denpasar sebagai Laboratorium Jejaring pemeriksa Covid-19 dari Kementerian Kesehatan Republik Indonesia nomor SR.01.07/II/2738/2021.<br>
                <i>Operational Permit for PCR Laboratory of Bhayangkara Denpasar Hospital as a COVID-19 examination laboratory from Minister of Health of The Republik of Indonesia number is SR.01.07/II/2738/2021.</i>
            </li>
            <li>
                Kode Lab :
                @if ($hs_kode_lab!="")
                    {{$hs_kode_lab}}
                @else
                    c.726
                @endif
                <br>
                <i>Lab Code </i>
            </li>
            <li>
                Diperiksa Oleh  :
                @if ($hs_diperiksa_oleh!="")
                    {{$hs_diperiksa_oleh}}
                @else
                    Analis
                @endif
                <br>
                <i>Checked by </i>
            </li>
            <li>
                Diverifikasi oleh :
                @if ($hs_diverifikasi_oleh!="")
                    {{$hs_diverifikasi_oleh}}
                @else
                    dr. Herry Herlambang Sp.PK
                @endif
                <br>
                <i>Verificated by </i>
            </li>
        </ul>
        <br>
        <table style="border:0; width:100%">
            <tr>
                <td style="text-align: right; width:14%"><img src="data:image/svg;base64, {!! $qrcode !!}"></td>
                <td style="font-size: 12px; width:80%">Silahkan scan QR-Code di samping untuk melakukan validasi terhadap dokumen hasil Swab Test ini. <br> 
                <i>Please scan the QR-Code to validate the results of this Swab Test.</i></td>
                <td></td>
            </tr>
        </table>
        
    </main>
    <htmlpagefooter name="page-footer">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_bawah.png") }}" alt="" style="width: 100%;" style="width: 120%; margin:0px -65px -40px -65px; ;">
    </htmlpagefooter>
    
    
</body>
</html>