@extends('layout.guest')

@section('title', 'Result Swab Test')

@section('content')
	<!-- Error page-->
    <div class="misc-wrapper"><a class="brand-logo" href="javascript:void(0);">
        <img src="{{ asset('app-assets/images/ico/logo-rs.png') }}" class="img-fluid" alt="Brand logo" style="width: 50px; height:50px;">
        <h2 class="brand-text text-success ml-1" style="padding-top:10px; ">E-Swab RS Bhayangkara Denpasar</h2>
    </a>
    <div class="misc-inner p-2 p-sm-3" style="margin-top:100px;">
        <div class="w-100 text-center">
            @if ($data1->status==0)
                <div class="card shadow-none bg-transparent border-danger">
                    <div class="card-body" style="margin:15px;">
                        <h4 class="card-title">Invalid Swab Test Results</h4>
                        <img src="{{ asset("app-assets/images/ico/uncheck.png") }}" alt="" style="width: 100px;">
                        <p class="card-text">The result of the swab test that you validate is not registered in our system!</p>
                    </div>
                </div>
            @else
                @foreach ($data as $item)
                    <div class="card shadow-none bg-transparent border-success text-center">
                        <div class="card-body" style="margin:15px;">
                            <h4 class="card-title">Swab Test Results</h4>
                            <img src="{{ asset("app-assets/images/ico/check.png") }}" alt="" style="width: 100px; height:100px;">
                            <p class="card-text">{{$item->hs_nama}} <br> Check Date : {{date("d/m/Y",strtotime($item->hs_tgl_periksa_spesimen))}} <br>
                                Specimen to : {{$item->hs_sampel_ke}} <br>
                                @if (strtolower($item->hs_hasil)=="negatif")
                                    <div disabled="" class="badge badge-md badge-success">NEGATIVE</div>
                                @else
                                    <div disabled="" class="badge badge-md badge-danger">POSITIVE</div>
                                @endif
                            </p>
                            @if ($data1->view=="surat")
                                <p class="card-text"><b>This letter is the original Swab result issued by Bhayangkara Hospital Denpasar</b></p>
                            @else
                                <a href="{{route('hasil-swab.generatepdf-en', $item->hs_code) }}" target="_blank"> <div disabled="" class="badge badge-md badge-primary">Download Swab Test Result</div></a>
                            @endif
                                <p class="text-center mt-2">
                                    <a href="/swabtest-result"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg> Back</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
                
            @endif
        </div>
    </div>
</div>
<!-- / Error page-->
@endsection
