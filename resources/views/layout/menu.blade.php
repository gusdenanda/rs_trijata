
<li class="nav-item {{ (request()->routeIs('dashboard.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{route('dashboard.index')}}"><i data-feather="home"></i><span class="menu-title text-truncate">Dashboard</span></a></li>

{{-- Data Master--}}
<li class="navigation-header"><span>Master</span><i data-feather="more-horizontal"></i></li>
<li class="nav-item {{ (request()->routeIs('kategori-pasien.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('kategori-pasien.index') }}"><i data-feather="box"></i><span class="menu-title text-truncate">Kategori Pasien</span></a></li>
<li class="nav-item {{ (request()->routeIs('subkategori-pasien.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('subkategori-pasien.index') }}"><i data-feather="package"></i><span class="menu-title text-truncate">Subkategori Pasien</span></a></li>
<li class="nav-item {{ (request()->routeIs('jenis-swab.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('jenis-swab.index') }}"><i data-feather="box"></i><span class="menu-title text-truncate">Jenis Swab</span></a></li>
<li class="nav-item {{ (request()->routeIs('penandatangan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('penandatangan.index') }}"><i data-feather="at-sign"></i><span class="menu-title text-truncate">Penandatangan</span></a></li>
<li class="nav-item {{ (request()->routeIs('user.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('user.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate">User</span></a></li>
<li class="nav-item {{ (request()->routeIs('setting.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('setting.index') }}"><i data-feather="database"></i><span class="menu-title text-truncate">Setting</span></a></li>

<li class="navigation-header"><span>Hasil Swab</span><i data-feather="more-horizontal"></i></li>
<li class="nav-item {{ (request()->routeIs('hasil-swab.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('hasil-swab.index') }}"><i data-feather="server"></i><span class="menu-title text-truncate">Hasil Swab</span></a></li>

<li class="navigation-header"><span>Report</span><i data-feather="more-horizontal"></i></li>
<li class="nav-item {{ (request()->routeIs('report-hasil-swab.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('report-hasil-swab.viewreport') }}"><i data-feather="book-open"></i><span class="menu-title text-truncate">Report Hasil Swab</span></a></li>

@if (request()->is('admin') || request()->is('admin/*'))
    {{-- Home --}}
    <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span class="menu-title text-truncate">Home</span></a></li>
    
    {{-- Data Master--}}
    <li class="navigation-header"><span>Master</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.user.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.user.index') }}"><i data-feather="user"></i><span class="menu-title text-truncate">User</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.reviewer.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.reviewer.index') }}"><i data-feather="user-check"></i><span class="menu-title text-truncate">Reviewer</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.author.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.author.index') }}"><i data-feather="user-plus"></i><span class="menu-title text-truncate">Author</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.kewarganegaraan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.kewarganegaraan.index') }}"><i data-feather="flag"></i><span class="menu-title text-truncate">Kewarganegaraan</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.faq.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.faq.index') }}"><i data-feather="help-circle"></i><span class="menu-title text-truncate">FAQ</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.portal.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.portal.index') }}"><i data-feather="aperture"></i><span class="menu-title text-truncate">Portal</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.conference.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin.master.conference.index') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">Conference</span></a></li>

@elseif (request()->is('admin-conference*'))
    {{-- Home --}}
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference"><i data-feather="home"></i><span class="menu-title text-truncate">Home</span></a></li>
    
    {{-- Data Conference --}}
    <li class="navigation-header"><span>Conference</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.conference.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin-conference.conference.conference.index') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">Conference</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.topik.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin-conference.conference.topik.index') }}"><i data-feather="activity"></i><span class="menu-title text-truncate">Topik</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.keynote.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ route('admin-conference.conference.keynote.index') }}"><i data-feather="key"></i><span class="menu-title text-truncate">Keynote Speaker</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/conference/tanggal-penting"><i data-feather="calendar"></i><span class="menu-title text-truncate">Tanggal Penting</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/conference/pengumuman"><i data-feather="info"></i><span class="menu-title text-truncate">Pengumuman</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/conference/sponsor"><i data-feather="dollar-sign"></i><span class="menu-title text-truncate">Sponsor</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/conference/voucher"><i data-feather="credit-card"></i><span class="menu-title text-truncate">Voucher</span></a></li>

    {{-- Data Submission --}}
    <li class="navigation-header"><span>Submission</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/submission"><i data-feather="file-text"></i><span class="menu-title text-truncate">Submission</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/submission/pembagian-reviewer"><i data-feather="users"></i><span class="menu-title text-truncate">Pembagian Review</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/submission/abstract"><i data-feather="book-open"></i><span class="menu-title text-truncate">Book of Abstract</span></a></li>

    {{-- Data Laporan --}}
    <li class="navigation-header"><span>Laporan</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/peserta-conference"><i data-feather="users"></i><span class="menu-title text-truncate">Peserta Conference</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/submission"><i data-feather="file-text"></i><span class="menu-title text-truncate">Submission</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/pembayaran"><i data-feather="shopping-bag"></i><span class="menu-title text-truncate">Pembayaran Author</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/published"><i data-feather="book"></i><span class="menu-title text-truncate">Published Article</span></a></li>

@elseif (request()->is('author*'))
    {{-- Home --}}
    <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span class="menu-title text-truncate">Home</span></a></li>
    
    {{-- Data Conference --}}
    <li class="navigation-header"><span>Author</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/author/profil"><i data-feather="user"></i><span class="menu-title text-truncate">Profil</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/author/submission"><i data-feather="file-text"></i><span class="menu-title text-truncate">Submission</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="book-open"></i><span class="menu-title text-truncate">Book of Abstract</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/author/conference"><i data-feather="cloud"></i><span class="menu-title text-truncate">Daftar Conference</span></a></li>
@endif