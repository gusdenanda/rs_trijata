@extends('layout.layout')

@section('title', 'Dashboard')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Pengumuman" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Judul</th>
						<th>Conference</th>
						<th>Tanggal</th>
                        <th>Jumlah Dilihat</th>
                        <th data-priority="2"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Contoh Pengumuman</td>
						<td>Contoh Conference</td>
						<td>20 Desember 2021</td>
						<td>15x</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning ubah"><i data-feather="edit-2"></i></button>
                                <button type="button" class="btn btn-danger deleteBtn" onclick="hapus(1)"><i data-feather="trash"></i></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <form id="delete-form" action="#" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<div class="modal-body">
				<x-form-group title="Judul">
					<input type="text" class="form-control" id="judul" placeholder="Judul Pengumuman" required/>
                </x-form-group>
                
                <x-form-group title="Conference">
					<select class="form-control" id="conference" required>
						<option value="" hidden>-- Pilih Conference --</option>
						<option value="1">Contoh Conference</option>
					</select>
                </x-form-group>
                
                <x-form-group title="Tanggal">
					<input type="text" class="form-control flatpickr-basic" placeholder="Tanggal Pengumuman" id="tanggal" required/>
                </x-form-group>
                
                <x-form-group title="Deskripsi">
                    <textarea class="form-control" id="deskripsi" rows="5" placeholder="Deskripsi Pengumuman" required></textarea>
                </x-form-group>

                <div id="tambah-gambar">
					<x-form-group title="Gambar">
						<input type="file" class="form-control-file" required/>
					</x-form-group>
				</div>
				<div id="ubah-gambar">
					Ini Contoh Gambar []
					<x-form-group title="Ubah Gambar">
						<input type="file" class="form-control-file" required/>
					</x-form-group>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
				<button type="button" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
				<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
			</div>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Date Picker
		$('.flatpickr-basic').flatpickr();

        // SweetAlert
        function hapus(id) {
            
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function (result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Deleted!',
                        text: 'Your file has been deleted.',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        },
                        showConfirmButton: false
                    });

                    $('#delete-form').attr('action', '{{ url("hapus") }}/' + id);
                    $('#delete-form').submit();
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: 'Cancelled',
                        text: 'Your imaginary file is safe :)',
                        icon: 'error',
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                }
            });
        }

		// Modal
		$(document).ready(function() {
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Keynote Speaker');

				$('#tambah-gambar').attr('hidden', false);
				$('#ubah-gambar').attr('hidden', true);

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$('.ubah').on('click', function() {
				$('.modal-title').text('Ubah Pengumuman');

				$('#tambah-gambar').attr('hidden', true);
				$('#ubah-gambar').attr('hidden', false);

				$('#judul').val('Contoh Judul');
				$('#conference').val(1);
				$('#tanggal').val('2021-02-27');
				$('#deskripsi').val('Contoh Deskripsi');

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#judul').val('');
				$('#ubah-gambar input').val('');
				$('#tambah-gambar input').val('');
				$('#conference').val('');
				$('#tanggal').val('');
				$('#deskripsi').val('');
			});
		});
	</script>
@endsection