<!DOCTYPE html>
<html>
<head>
    <title>Hasil SWAB {{$hs_nama}}</title>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 60px;
            header: page-header;
	        footer: page-footer;
        }
        header {
            position: fixed;
            top: -100px;
            left: -65px;
            right: -65px;
            height: 70px;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed; 
            bottom: -60px; 
            left: -65px; 
            right: -65px;
            height: 90px; 

            /** Extra personal styles **/
            text-align: center;
            line-height: 35px;
        }
        .tableborder {
            border-left: 0.01em solid #000;
            border-right: 0;
            border-top: 0.01em solid #000;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table {
            border-collapse: collapse;
        }
        .tabletdborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0;
            border-bottom: 0.01em solid #000;
            padding: 5px;
        }
    
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_atas.png") }}" alt="" style="width: 120%; margin:-40px -65px 0px -65px; ;">
    </htmlpageheader>
    <main>
        <br><br><br>
        @if ($js_id=="2")
            <p style="text-align: center; font-size:20px; line-height: 10px; font-weight:bold;"><u>HASIL PEMERIKSAAN LABORATORIUM</u></p>
        @else
            <p style="text-align: center; font-size:20px; line-height: 10px; font-weight:bold;"><u>HASIL PEMERIKSAAN COVID-19</u></p>
            <p style="text-align: center; font-size:20px;  line-height: 10px; font-weight:bold;"><i>Metode Real-Time PCR<i></p>
        @endif
        <br>
        <table style="width:100%">
            <tr>
                <td>
                    <table style="width:100%; border:none; font-size:16px;">
                        @if ($js_id!="1")
                            <tr>
                                <td>Sampel ID</td>
                                <td>:</td>
                                <td>{{$hs_nomor_spesimen_antigen}}</td>
                            </tr>
                            <tr>
                                <td>Tgl Periksa</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_periksa_spesimen))}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td style="width:30%; border:none;">Nama Pasien</td>
                            <td style="width:3%; border:none;">:</td>
                            <td style="border:none;">{{$hs_nama}}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td>:</td>
                            <td>{{date("d/m/Y",strtotime($hs_tgl_lahir))}}</td>
                        </tr>
                        <tr>
                            <td>Umur / Kelamin</td>
                            <td>:</td>
                            <td>{{$hs_usia}} Thn / {{ucwords($hs_jk)}}</td>
                        </tr>
                        @if ($js_id=="1")
                            <tr>
                                <td valign="top">Alamat</td>
                                <td valign="top">:</td>
                                <td>{{$hs_alamat}}</td>
                            </tr>
                            <tr>
                                <td>No Tlp</td>
                                <td>:</td>
                                <td>{{$hs_no_hp}}</td>
                            </tr>
                            <tr>
                                <td>Tgl Pengambilan Spesimen</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_kirim_spesimen))}}</td>
                            </tr>
                            <tr>
                                <td>Tgl Spesimen Selesai</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_periksa_spesimen))}}</td>
                            </tr>
                            <tr>
                                <td>Spesimen ke</td>
                                <td>:</td>
                                <td>{{$hs_sampel_ke}}</td>
                            </tr>
                            <tr>
                                <td>Hasil</td>
                                <td>:</td>
                                @if (strtolower($hs_hasil)=="positif")
                                    <td style="color:#FF0000;">{{ucwords($hs_hasil)}}</td>
                                @else
                                    <td>{{ucwords($hs_hasil)}}</td>
                                @endif
                            </tr>
                            @if ($hs_ct_value!="")
                                <tr>
                                    <td>CT Value</td>
                                    <td>:</td>
                                    @if ($hs_ct_value > $ct_value)
                                        <td>Tidak terdeteksi (Cut off ≤ {{$ct_value}})</td>
                                    @else
                                        <td>{{$hs_ct_value}} (Cut off ≤ {{$ct_value}})</td>
                                    @endif
                                </tr>
                            @endif
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <br><br>
        @if ($js_id=="2")
            <table style="width:100%; font-size:18px;" class="tableborder">
                <tr>
                    <td style="width: 40%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">PARAMETER</td>
                    <td style="width: 30%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">HASIL</td>
                    <td style="text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">NILAI NORMAL</td>
                </tr>
                <tr>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Covid-19 Antigen Rapid Test</td>
                    @if (strtolower($hs_hasil)=="positif")
                        <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @else
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @endif
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negatif</td>
                </tr>
            </table><br><br>
            <table style="width:100%; font-size:12px;" class="tableborder">
                <tr>
                    <td style="width:45%; text-align:justify" valign="top" class="tabletdborder">
                       @if (strtolower($hs_hasil)=="negatif")
                            <table style="width:100%; font-size:12px;" class="tableborder" style="border-left: 0;
                                    border-right: 0;
                                    border-top: 0.01em solid #ffffff;
                                    border-bottom: 0;
                                    border-collapse: collapse;">
                                <tr>
                                    <td style="width:7%" valign="top">1.</td>
                                    <td>Hasil Negatif tidak menyingkirkan kemungkinan terinfeksi SARS-CoV-2 sehingga masih beresiko menularkan ke orang lain, 
                                        disarankan tes ulang atau tes konfirmasi dengan NAAT, bila probilitas pretest relatif tinggi terutama bila pasien bergejala
                                        atau diketahui memiliki kontak orang yang terkonfirmasi Covid-19; </td>
                                </tr>
                                <tr>
                                    <td> 2.</td>
                                    <td>Hasil Negatif dapat terjadi pada kondisi kuantitas antigen pada spesimen di bawah level deteksi alat.</td>
                                </tr>
                            </table>
                       @else
                            <table style="width:100%; font-size:12px;" class="tableborder" style="border-left: 0;
                                border-right: 0;
                                border-top: 0.01em solid #ffffff;
                                border-bottom: 0;
                                border-collapse: collapse;">
                                <tr>
                                    <td style="width:7%" valign="top">1.</td>
                                    <td>Pemeriksaan konfirmasi dengan pemeriksaan RT-PCR;</td>
                                </tr>
                                <tr>
                                    <td> 2.</td>
                                    <td>Lakukan karantina atau isolasi sesuai dengan kriteria;</td>
                                </tr>
                                <tr>
                                    <td> 3.</td>
                                    <td>Menerapkan PHBS, etika batuk, menggunakan masker saat sakit, jaga stamina dan physical distancing.</td>
                                </tr>
                            </table>
                       @endif
                    </td>
                    <td style="text-align:center; width:25%" valign="top" class="tabletdborder">
                        Pemeriksa<br><br>
                        @if ($pemeriksa_ttd!="")
                            <img src="{{ public_path("images/".$pemeriksa_ttd."") }}" alt="" style="width: 120px; height:80px; margin-left:-30px;"> <br>
                        @endif
                        <u>Analis</u>
                    </td>
                    <td style="text-align:center" valign="top" class="tabletdborder">
                        dr. Patologi Klinik<br><br>
                        @if ($ps_ttd!="")
                            <img src="{{ public_path("images/".$ps_ttd."") }}" alt="" style="width: 400px; height:80px; z-index:-1; margin-top:-10px; margin-left:-90px; margin-right:10px;"> <br>
                        @endif
                        <u>{{$ps_nama}}</u><br>
                        {{$ps_pangkat}} NRP {{$ps_nrp}}
                    </td>
                </tr>
            </table>
        @else
            <!--<table style="width:100%; font-size:16px;" class="tableborder">
                <tr>
                    <td style="width: 30%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">JENIS PEMERIKSAAN</td>
                    <td style="width: 25%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">HASIL</td>
                    <td style="width: 25%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">NILAI RUJUKAN</td>
                    <td style="text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">METODE</td>
                </tr>
                <tr>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">SARS-CoV-2</td>
                    @if (strtolower($hs_hasil)=="positif")
                        <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @else
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @endif
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negatif</td>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">RT-PCR</td>
                </tr>
            </table><br><br><br><br>-->
            <table style="border:0; width:100%">
                <tr>
                    <td style="width:45%; text-align:center" valign="top">
                        Dokter Penanggung Jawab<br><br>
                        @if ($ps_ttd!="")
                            <img src="{{ public_path("images/".$ps_ttd."") }}" alt="" style="width: 250px; height:80px"> <br>
                        @endif
                        <u>{{$ps_nama}}</u><br>
                        {{$ps_pangkat}} NRP {{$ps_nrp}}
                    </td>
                    <td style="width:15%" valign="top"></td>
                    <td style="text-align:center" valign="top">Pemeriksa<br><br>
                        @if ($pemeriksa_ttd!="")
                            <img src="{{ public_path("images/".$pemeriksa_ttd."") }}" alt="" style="width: 150px; height:80px"> <br>
                        @endif
                        <u>{{$pemeriksa_nama}} </u>
                    </td>
                </tr>
            </table>
        @endif
        
        <br><br>
        <table style="border:0; width:100%">
            <tr>
                <td style="text-align: right; width:14%"><img src="data:image/svg;base64, {!! $qrcode !!}"></td>
                <td style="font-size: 12px; width:50%">Silahkan scan QR-Code di samping untuk melakukan validasi terhadap dokumen hasil Swab Test ini. </td>
                <td></td>
            </tr>
            @if ($js_id=="1")
                <tr>
                    <td colspan="3"></td>
                <tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Catatan :</td>
                <tr>
                <tr>
                    <td style="text-align: right;" valign="top">-</td>
                    <td style="font-size: 10px; " valign="top" colspan="2">Pemeriksaan dilakukan dengan metode Realtime PCR dan hanya menggambarkankondisi saat pengambilan specimen.</td>
                </tr>
                <tr>
                    <td style="text-align: right;" valign="top">-</td>
                    <td style="font-size: 10px; " valign="top" colspan="2">Ijin Oprasional Laboratorium PCR RS Bhayangkara Denpasar sebagai laboratorium pemeriksa COVID-19 dari Kementrian Kesehatan Republik Indonesia nomor  SR.01.07/II/2738/2021.</td>
                </tr>
            @endif
        </table>
        
    </main>
    <htmlpagefooter name="page-footer">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_bawah.png") }}" alt="" style="width: 100%;" style="width: 120%; margin:0px -65px -40px -65px; ;">
    </htmlpagefooter>
    
    
</body>
</html>