@extends('layout.layout')

@section('title', 'Hasil Swab')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <div class="card">
			<form name="formcheck" id="formcheck" method="post" action="{{ route('hasil-swab.generatepdfmultiple') }}">
				@csrf
				<div id="tabel_judul" hidden>Data Hasil Swab</div>
				<div id="id" hidden>import</div>
				<div id="id1" hidden>tambah</div>
				<div id="id3" hidden>print</div>
				<div id="button_title" hidden><i data-feather="plus"></i> Import Excel</div>
				<div id="button_title2" hidden><i data-feather="plus"></i> Tambah Data</div>
				<div id="button_title3" hidden><i data-feather="print"></i> Print Multiple</div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th data-priority="1">Pilih</th>
							<th>Nama</th>
							<th>NIK</th>
							<th>Tanggal Lahir</th>
							<th>Nomor HP</th>
							<th>Tanggal Periksa</th>
							<th>Hasil</th>
							<th>Satker</th>
							<th>Subkategori Pasien</th>
							<th>Jenis Test</th>
							<th data-priority="2">Aksi</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</form>
		</div>	
            
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
            <form name="form" id="form" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <x-form-group title="Nama *">
						<input type="text" class="form-control" id="hs_nama" name="hs_nama" placeholder="Nama" required/>
					</x-form-group>
                    <x-form-group title="NIK *">
						<input type="text" class="form-control" id="hs_nik" name="hs_nik" placeholder="NIK" required/>
					</x-form-group>
                    <x-form-group title="Tanggal Lahir *">
                        <input type="text" class="form-control flatpickr-basic" id="hs_tgl_lahir" name="hs_tgl_lahir" placeholder="Tanggal Lahir"/>
                    </x-form-group>
                    <x-form-group title="Jenis Kelamin *">
                        <select class="form-control" id="hs_jk" name="hs_jk" required>
                            <option value="" hidden>-- Pilih Jenis Kelamin --</option>
                            <option value="laki-laki">Laki-laki</option>
                            <option value="perempuan">Perempuan</option>
                        </select>
                    </x-form-group>
                    <x-form-group title="No. Telp / HP *">
						<input type="text" class="form-control" id="hs_no_hp" name="hs_no_hp" placeholder="NO. Telp/HP" required/>
					</x-form-group>
					<x-form-group title="Alamat">
						<textarea class="form-control" id="hs_alamat" name="hs_alamat" placeholder="Alamat" rows="4"></textarea>
					</x-form-group>
                    <x-form-group title="Keterangan Faskes">
						<input type="text" class="form-control" id="hs_faskes" name="hs_faskes" placeholder="Keterangan Faskes" />
					</x-form-group>
                    <x-form-group title="Sampel ke-">
						<input type="text" pattern="[0-9]*" id="hs_sampel_ke" name="hs_sampel_ke" title="Hanya boleh diisi dengan angka(0-9)" class="form-control" placeholder="Sampel ke-"/>
					</x-form-group>
                    <x-form-group title="Jenis Swab *">
						<select class="form-control" id="js_id" name="js_id" required>
							<option value="" hidden>-- Jenis Swab --</option>
							@foreach ($jenis_swab as $item)
								<option value="{{ $item->id }}">{{ $item->js_nama }}</option>
							@endforeach
						</select>
					</x-form-group>
                    <x-form-group title="Nomor Spesimen">
						<input type="text" class="form-control" id="hs_nomor_spesimen" name="hs_nomor_spesimen" placeholder="Nomor Spesimen"/>
					</x-form-group>
                    <x-form-group title="Tanggal Kirim Spesimen">
                        <input type="text" class="form-control flatpickr-basic" id="hs_tgl_kirim_spesimen" name="hs_tgl_kirim_spesimen" placeholder="Tanggal Kirim Spesimen"/>
                    </x-form-group>
					<x-form-group title="Jam Kirim Spesimen">
                        <input type="text" class="form-control flatpickr-basic-time" id="hs_jam_kirim_spesimen" name="hs_jam_kirim_spesimen" placeholder="Jam Kirim Spesimen"/>
                    </x-form-group>
                    <x-form-group title="Tanggal Terima Spesimen">
                        <input type="text" class="form-control flatpickr-basic" id="hs_tgl_terima_spesimen" name="hs_tgl_terima_spesimen" placeholder="Tanggal Terima Spesimen"/>
                    </x-form-group>
					<x-form-group title="Jam Terima Spesimen">
                        <input type="text" class="form-control flatpickr-basic-time" id="hs_jam_terima_spesimen" name="hs_jam_terima_spesimen" placeholder="Jam Terima Spesimen"/>
                    </x-form-group>
                    <x-form-group title="Tanggal Periksa Spesimen *">
                        <input type="text" class="form-control flatpickr-basic" id="hs_tgl_periksa_spesimen" name="hs_tgl_periksa_spesimen" placeholder="Tanggal Periksa Spesimen required"/>
						<input type="hidden" class="form-control" id="hs_tgl_periksa_spesimen_old" name="hs_tgl_periksa_spesimen_old"/>
                    </x-form-group>
					<x-form-group title="Jam Periksa Spesimen">
                        <input type="text" class="form-control flatpickr-basic-time" id="hs_jam_periksa_spesimen" name="hs_jam_periksa_spesimen" placeholder="Jam Periksa Spesimen"/>
						</x-form-group>
                    <x-form-group title="Hasil *">
                        <select class="form-control" id="hs_hasil" name="hs_hasil" required>
                            <option value="" hidden>-- Hasil SWAB --</option>
                            <option value="positif">Positif</option>
                            <option value="negatif">Negatif</option>
                            <option value="inconclusive">Inconclusive</option>
                        </select>
                    </x-form-group>
					<x-form-group title="CT Value">
						<input type="text" class="form-control" id="hs_ct_value" name="hs_ct_value" placeholder="CT Value" />
					</x-form-group>
                    <x-form-group title="Kriteria">
						<input type="text" class="form-control" id="hs_kriteria" name="hs_kriteria" placeholder="Kriteria" />
					</x-form-group>
                    <x-form-group title="Satker">
						<input type="text" class="form-control" id="hs_satker" name="hs_satker" placeholder="Satker" />
					</x-form-group>
                    <x-form-group title="Kategori Pasien *">
						<select class="form-control" id="skp_id" name="skp_id" required>
							<option value="" hidden>-- Pilih Kategori Pasien --</option>
							@foreach ($subkategori_pasien as $item)
								<option value="{{ $item->id }}">{{ $item->skp_nama }}</option>
							@endforeach
						</select>
					</x-form-group>
					<x-form-group title="Kode Lab">
						<input type="text" class="form-control" id="hs_kode_lab" name="hs_kode_lab" placeholder="Kode Lab"  />
					</x-form-group>
					<x-form-group title="Diperiksa Oleh">
						<input type="text" class="form-control" id="hs_diperiksa_oleh" name="hs_diperiksa_oleh" placeholder="Diperiksa Oleh" />
					</x-form-group>
					<x-form-group title="Diverifikasi Oleh">
						<input type="text" class="form-control" value="{{$dr_pj->ps_nama}}" id="hs_diverifikasi_oleh" name="hs_diverifikasi_oleh" placeholder="Diverifikasi Oleh" />
					</x-form-group>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                </div>
            </form>
		</x-modal>

        <x-modal title="" type="normal" class="" id="modalimport">
            <form name="formimport" id="formimport" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <x-form-group title="File Excel">
						<input type="file" class="form-control" id="file" name="file"required/>
					</x-form-group>
                    Untuk import data dengan file excel silahkan sesuaikan dengan template excel di bawah ini. <br>
					Perhatikan juga, bahwa untuk status harus diganti terlebih dahulu menjadi kode sesuai dengan kode yang ada pada Menu Subkategori Pasien<br>
					Format Tanggal juga harus sesuai dengan yang sudah ditetapkan pada template. <br>
                    <a href="{{ url("app-assets/data/template-excel-new.xlsx") }}">
                        <img src="{{ asset("app-assets/images/ico/excel-icon.png") }}" alt="" style="width: 50px;"> 
						[Download Template]
                    </a>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                </div>
            </form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    {{-- Datatable --}}
	<script>
		var link = "{{ route('hasil-swab.data') }}";
		var column = [
			{data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
			{data: 'hs_nama', name: 'hs_nama'},
			{data: 'hs_nik', name: 'hs_nik'},
			{data: 'hs_tgl_lahir', name: 'hs_tgl_lahir'},
			{data: 'hs_no_hp', name: 'hs_no_hp'},
			{data: 'hs_tgl_periksa_spesimen', name: 'hs_tgl_periksa_spesimen'},
			{data: 'hs_hasil', name: 'hs_hasil'},
			{data: 'hs_satker', name: 'hs_satker'},
			{data: 'skp_nama', name: 'skp_nama', orderable: false, searchable: false},
			{data: 'js_id', name: 'js_id'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button-swab.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.flatpickr-basic-time').flatpickr({
				allowInput: true,
				enableTime: true,
				noCalendar: true,
				dateFormat: "H:i:S",
				time_24hr: true
			});
			$('.flatpickr-basic').flatpickr({
				allowInput: true,
			});
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Hasil Swab');
                $('#form').attr('action', "{{ url('hasil-swab') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				$.get( "{{ url('hasil-swab') }}/generatenomor", function( data ) {
					//var d = JSON.parse(data);
					$('#hs_nomor_spesimen').val(data);
				});
				$('#modal').modal('show');
			});

            $('#import').on('click', function() {
				$('.modal-title').text('Import File Excel');
                $('#formimport').attr('action', "{{ url('hasil-swab/import_excel') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				
				$('#modalimport').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('hasil-swab') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Hasil Swab');
					$('#form').attr('action', "{{ url('hasil-swab') }}/" + id);

					$('#hs_nama').val(d.hs_nama);
					$('#hs_nik').val(d.hs_nik);
					$('#hs_tgl_lahir').val(d.hs_tgl_lahir);
					$('#hs_jk').val(d.hs_jk);
					$('#hs_no_hp').val(d.hs_no_hp);
					$('#hs_alamat').val(d.hs_alamat);
					$('#hs_faskes').val(d.hs_faskes);
					$('#hs_sampel_ke').val(d.hs_sampel_ke);
					$('#js_id').val(d.js_id);
					if(d.js_id=='1'){
						$('#hs_nomor_spesimen').val(d.hs_nomor_spesimen);
					}else{
						$('#hs_nomor_spesimen').val(d.hs_nomor_spesimen_antigen);
					}
					$('#hs_tgl_kirim_spesimen').val(d.hs_tgl_kirim_spesimen);
					$('#hs_tgl_terima_spesimen').val(d.hs_tgl_terima_spesimen);
					$('#hs_tgl_periksa_spesimen').val(d.hs_tgl_periksa_spesimen);
					$('#hs_tgl_periksa_spesimen_old').val(d.hs_tgl_periksa_spesimen);
					$('#hs_hasil').val(d.hs_hasil);
					$('#hs_kriteria').val(d.hs_kriteria);
					$('#hs_satker').val(d.hs_satker);
					$('#skp_id').val(d.skp_id);
					$('#hs_ct_value').val(d.hs_ct_value);
					$('#hs_jam_kirim_spesimen').val(d.hs_jam_kirim_spesimen);
					$('#hs_jam_terima_spesimen').val(d.hs_jam_terima_spesimen);
					$('#hs_jam_periksa_spesimen').val(d.hs_jam_periksa_spesimen);
					$('#hs_kode_lab').val(d.hs_kode_lab);
					$('#hs_diperiksa_oleh').val(d.hs_diperiksa_oleh);
					$('#hs_diverifikasi_oleh').val(d.hs_diverifikasi_oleh);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				$('#hs_nama').val('');
				$('#hs_nik').val('');
				$('#hs_tgl_lahir').val('');
				$('#hs_jk').val('');
				$('#hs_no_hp').val('');
				$('#hs_alamat').val('');
				$('#hs_faskes').val('');
				$('#hs_sampel_ke').val('');
				$('#js_id').val('');
				$('#hs_nomor_spesimen').val('');
				$('#hs_tgl_periksa_spesimen_old').val('');
				$('#hs_tgl_kirim_spesimen').val('');
				$('#hs_tgl_terima_spesimen').val('');
				$('#hs_tgl_periksa_spesimen').val('');
				$('#hs_hasil').val('');
				$('#hs_kriteria').val('');
				$('#hs_satker').val('');
				$('#skp_id').val('');
				$('#hs_ct_value').val('');
				$('#hs_jam_kirim_spesimen').val('');
				$('#hs_jam_terima_spesimen').val('');
				$('#hs_jam_periksa_spesimen').val('');
				$('#hs_kode_lab').val('');
				$('#hs_diperiksa_oleh').val('');
				$('#hs_diverifikasi_oleh').val('');
			});

			// Tambah/Ubah Data
			$('#formimport').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modalimport').modal('hide');
					formExecuted(response);
				});
			});

            $('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("hasil-swab")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection