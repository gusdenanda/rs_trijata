@extends('layout.guest')

@section('title', 'Login')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/page-auth.css')}}">
@endsection

@section('content')
	<!-- Error page-->
    <div class="auth-wrapper auth-v2">
        <div class="auth-inner row m-0">
            <a class="brand-logo" href="#">
                <img src="{{ asset('app-assets/images/ico/logo-rs.png') }}" class="img-fluid" alt="Brand logo" style="width: 50px; height:50px;">
                <h4 class="brand-text text-success ml-1" style="padding-top:15px; ">E-Swab RS Bhayangkara Denpasar</h4>
            </a>
            <div class="d-none d-lg-flex col-lg-6 align-items-center p-5">
                <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="{{asset('app-assets/images/pages/register-v2.svg')}}" alt="Login" /></div>
            </div>
            <!-- /Left Text-->
            <!-- Login-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto" style="margin-top:100px;">
                    <img src="{{ asset('app-assets/images/ico/logo-rstrijata.png') }}" class="img-fluid"> <br><br>
                    <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>
                    <form class="auth-login-form mt-2" action="{{ route('login') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label class="form-label" for="login-email">Email</label>
                            <input class="form-control" id="login-email" type="text" name="email" placeholder="john@example.com" aria-describedby="login-email" autofocus="" tabindex="1" />
                        </div>
                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                <label for="login-password">Password</label>
                            </div>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input class="form-control form-control-merge" id="login-password" type="password" name="password" placeholder="············" aria-describedby="login-password" tabindex="2" />
                                <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" name="remember" id="remember-me" type="checkbox" tabindex="3" />
                                <label class="custom-control-label" for="remember-me"> Remember Me</label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                    </form>
                    <div class="divider my-2">
                        <div class="divider-text">or</div>
                    </div>
                    <div class="auth-footer-btn d-flex justify-content-center">
                        <a href="{{route('hasil-swab.formsearch')}}"><button class="btn btn-success btn-block" tabindex="5">Cek Hasil SWAB Test</button></a>
                    </div>
                </div>
            </div>
            <!-- /Login-->
        </div>
    </div>
<!-- / Error page-->
@endsection
