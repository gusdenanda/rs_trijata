@extends('layout.layout')

@section('title', 'Penandatangan')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Penandatangan" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Nama Lengkap</th>
                        <th>NRP</th>
                        <th>PAngkat/Jabatan</th>
						<th>Status</th>
						<th >Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>

            <form id="delete-form" action="#" method="POST" class="d-none">
                @csrf
                @method('DELETE')
            </form>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
            <form name="form" id="form" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <x-form-group title="Nama Lengkap">
                        <input type="text" class="form-control" name="ps_nama" id="ps_nama" placeholder="Nama Lengkap" required/>
                    </x-form-group>
                    <x-form-group title="NRP">
                        <input type="text" class="form-control" name="ps_nrp" id="ps_nrp" placeholder="NRP"/>
                    </x-form-group>
                    <x-form-group title="Pangkat/Jabatan">
                        <input type="text" class="form-control" name="ps_pangkat" id="ps_pangkat" placeholder="Pangkat/Jabatan"/>
                    </x-form-group>
                    <x-form-group title="Penandatangan">
						<select class="form-control" id="ps_penandatangan" name="ps_penandatangan" required>
							<option value="" hidden>-- Penandatangan Untuk --</option>
							<option value="1">Dokter Penanggung Jawab</option>
							<option value="2">Petugas Pemeriksa</option>
							<option value="3">Rekapan Laporan Hasil Swab</option>
						</select>
					</x-form-group>
                    <x-form-group title="Status">
						<div>
							<input type="radio" id="ps_active_1" name="ps_active"  value="1" />
							<label for="radio_1">Aktif</label>
						</div>
				
						<div>
							<input type="radio" id="ps_active_0" name="ps_active"  value="0" />
							<label for="radio_2">Tidak Aktif</label>
						</div>
					</x-form-group>
					<x-form-group title="Scan TTD">
						<input type="file" class="form-control-file" accept="image/*" name="ttd" id="ttd"/>
					</x-form-group>
                    <div id="filettd"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                    <button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
                    <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    <button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
                </div>
            </form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    {{-- Datatable --}}
	<script>
		var link = "{{ route('penandatangan.data') }}";
		var column = [
			{data: 'ps_nama', name: 'ps_nama'},
			{data: 'ps_nrp', name: 'ps_nrp'},
			{data: 'ps_pangkat', name: 'ps_pangkat'},
            {data: 'ps_status', name: 'ps_status', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Data Penandatangan');
                $('#form').attr('action', "{{ url('penandatangan') }}");
				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");
                $('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$.get( "{{ url('penandatangan') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Penandatangan');
					$('#form').attr('action', "{{ url('penandatangan') }}/" + id);

					$('#ps_nama').val(d.ps_nama);
					$('#ps_nrp').val(d.ps_nrp);
					$('#ps_pangkat').val(d.ps_pangkat);
					$('#ps_penandatangan').val(d.ps_penandatangan);
					$('#ps_active_'+ d.ps_active).prop('checked', true);
                    if(d.ps_ttd!=''){
                        $('#filettd').append('<img src="repositories/public_html/images/'+ d.ps_ttd +'" class="img-fluid">');
                    }
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#form').attr('action', '');
                $('#method').remove();
				$('#ps_nama').val('');
				$('#ps_nrp').val('');
				$('#ps_pangkat').val('');
				$('#ps_penandatangan').val('');
                $('#ps_active_1').prop('checked', true);
				$('#ps_active_0').prop('checked', false);
                $('#filettd').children().remove();
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					$('#modal').modal('hide');
					formExecuted(response);
				});
			});
		});

        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("penandatangan")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection