<!DOCTYPE html>
<html>
<head>
    <title>Hasil SWAB</title>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 40px 40px;
        }
        header {
            position: fixed;
            top: -100px;
            left: -65px;
            right: -65px;
            height: 70px;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed; 
            bottom: -60px; 
            left: -65px; 
            right: -65px;
            height: 50px; 

            /** Extra personal styles **/
            text-align: center;
            line-height: 35px;
        }
        .tableborder {
            border-left: 0.01em solid #000;
            border-right: 0;
            border-top: 0.01em solid #000;
            border-bottom: 0;
            border-collapse: collapse;
            border-spacing: 30px;
        }
        .tabletdborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0;
            border-bottom: 0.01em solid #000;
            padding:3px 7px;
        }
        .tablethborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0;
            border-bottom: 0.01em solid #000;
            padding:3px 7px;
        }
    </style>
</head>
<body>
    <main>
        <div style="text-align: center">
            <table class="table-bordereless" style="width: 80%">
                <tr>
                    <td rowspan="5" style="width: 30%; text-align:right"><img src="{{ public_path('app-assets/images/ico/logo-rs.png') }}" class="img-fluid"style="width: 100px;"></td>
                    <td style="font-size: 18px;text-align: center"><b>RUMAH SAKIT</b></td>
                </tr>
                <tr>
                    <td style="font-size: 16px;text-align: center;"><b>BHAYANGKARA DENPASAR</b></td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 12px;">Jln. Trijata No. 32 Denpasar - Bali</td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 12px;">Telp. (0361) 222908 - 234670; Fax. (0361) 238235; Customer Service 081236236838</td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 12px;">Website: eswab.rstrijata.com, Email: rstrijata@gmail.com</td>
                </tr>
            </table>
        </div>
        <hr><br>
        <div class="table-responsive">
            <table class="table tableborder" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th data-priority="1" class="tablethborder" style="text-align: center; font-weight:bold;">No.</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Nama</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">NIK</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Jenis Kelamin</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Tanggal Lahir</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Usia</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Nomor HP</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Alamat</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Faskes</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Tanggal Kirim</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Tanggal Terima</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Tanggal Periksa</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Hasil</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Satker</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Kategori Pasien</th>
                        <th class="tablethborder" style="text-align: center; font-weight:bold;">Jenis Pemeriksaan</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $p=1;
                    @endphp
                    @foreach ($hasil_swab as $hs)
                        <tr>
                            <td class="tabletdborder" style="text-align: center;">{{$p}}</td>
                            <td class="tabletdborder">{{$hs->hs_nama}}</td>
                            <td class="tabletdborder">{{$hs->hs_nik}}</td>
                            <td class="tabletdborder">{{ucwords($hs->hs_jk)}}</td>
                            <td class="tabletdborder">{{date("d-m-Y",strtotime($hs->hs_tgl_lahir))}}</td>
                            <td class="tabletdborder" style="text-align: center;">{{$hs->hs_usia}} Thn</td>
                            <td class="tabletdborder">{{$hs->hs_no_hp}}</td>
                            <td class="tabletdborder">{{$hs->hs_alamat}}</td>
                            <td class="tabletdborder">{{$hs->hs_faskes}}</td>
                            @if (date("d-m-Y",strtotime($hs->hs_tgl_kirim_spesimen))=="01-01-1970")
                                <td class="tabletdborder"></td>
                            @else
                                <td class="tabletdborder">{{date("d-m-Y",strtotime($hs->hs_tgl_kirim_spesimen))}}</td>
                            @endif
                            @if (date("d-m-Y",strtotime($hs->hs_tgl_terima_spesimen))=="01-01-1970")
                                <td class="tabletdborder"></td>
                            @else
                                <td class="tabletdborder">{{date("d-m-Y",strtotime($hs->hs_tgl_terima_spesimen))}}</td>
                            @endif
                            @if (date("d-m-Y",strtotime($hs->hs_tgl_periksa_spesimen))=="01-01-1970")
                                <td class="tabletdborder"></td>
                            @else
                                <td class="tabletdborder">{{date("d-m-Y",strtotime($hs->hs_tgl_periksa_spesimen))}}</td>
                            @endif
                            @if ($hs->hs_hasil=="positif")
                                <td class="tabletdborder" style="color: #ff0000">{{ucwords($hs->hs_hasil)}}</td>
                            @else
                                <td class="tabletdborder" style="color: #008000">{{ucwords($hs->hs_hasil)}}</td>
                            @endif
                            <td class="tabletdborder">{{$hs->hs_satker}}</td>
                            <td class="tabletdborder">{{$hs->skp_nama}}</td>
                            @if ($hs->js_id=="1")
                                <td class="tabletdborder">Swab PCR</td>
                            @else
                                <td class="tabletdborder">Swab Antigen</td>
                            @endif
                        </tr>
                        @php
                            $p++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
            <br><br>
            <table style="border:0; width:100%">
                <tr>
                    <td style="width:35%; text-align:center" valign="top">
                        
                    </td>
                    <td style="width:35%" valign="top"></td>
                    <td style="text-align:center" valign="top">
                        Dokter Penanggung Jawab<br><br>
                        @if ($dr_pj->ps_ttd!="")
                            <img src="{{ public_path("images/".$dr_pj->ps_ttd."") }}" alt="" style="width: 250px; height:80px"> <br>
                        @endif
                        <u>{{$dr_pj->ps_nama}}</u><br>
                        {{$dr_pj->ps_pangkat}} NRP {{$dr_pj->ps_nrp}}
                    </td>
                </tr>
            </table>
        </div>
    </main>
</body>
</html>