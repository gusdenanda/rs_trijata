@extends('layout.layout')

@section('title', 'Hasil Swab')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
    <div class="row" id="table-bordered">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Laporan Hasil SWAB Test</h4>
                </div>
                <div class="card-body">
                    <form class="dt_adv_search" method="POST" action="{{route('report-hasil-swab.viewreport')}}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label>Tanggal Pemeriksaan Spesimen:</label>
                                        <div class="form-group mb-0">
                                            @if ($data->from_date!="")
                                                <input type="text" class="form-control flatpickr-basic" value="{{$data->from_date}}" id="from_tanggal" name="from_date" placeholder="Tanggal Periksa Spesimen"/>
                                            @else
                                                <input type="text" class="form-control flatpickr-basic" id="from_date" name="from_date" value="{{date("Y-m-d")}}"/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Sampai tanggal</label>
                                        <div class="form-group mb-0">
                                            @if ($data->to_date!="")
                                                <input type="text" class="form-control flatpickr-basic" value="{{$data->to_date}}" id="to_date" name="to_date" />
                                            @else
                                                <input type="text" class="form-control flatpickr-basic" id="to_date" name="to_date" value="{{date("Y-m-d")}}"/>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Kategori Pasien:</label>
                                        <select class="form-control" id="skp_id" name="skp_id" >
                                            <option value="">-- Pilih Kategori Pasien --</option>
                                            @foreach ($subkategori_pasien_master as $item)
                                                @if ($item->id==$data->skp_id)
                                                    <option value="{{ $item->id }}" selected="selected">{{ $item->skp_nama }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{{ $item->skp_nama }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Jenis Pemeriksaan:</label>
                                        <select class="form-control" id="js_id" name="js_id" >
                                            <option value="">-- Pilih Jenis Pemeriksaan --</option>
                                            @foreach ($js_id as $item)
                                                @if ($item->id==$data->js_id)
                                                    <option value="{{ $item->id }}" selected="selected">{{ $item->js_nama }}</option>
                                                @else
                                                    <option value="{{ $item->id }}">{{ $item->js_nama }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-4">
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px;">Cari...</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
                <form action="{{route('report-hasil-swab.cetakpdf')}}" method="post">
                    @csrf
                    @if ($data->from_date!="")
                        <input type="hidden" name="from_date" value={{$data->from_date}}>
                    @else
                        <input type="hidden" name="from_date" value=>
                    @endif
                    @if ($data->to_date!="")
                        <input type="hidden" name="to_date" value={{$data->to_date}}>
                    @else
                        <input type="hidden" name="to_date" value=>
                    @endif
                    @if ($data->skp_id!="")
                        <input type="hidden" name="skp_id" value={{$data->skp_id}}>
                    @else
                        <input type="hidden" name="skp_id" value=>
                    @endif
                     @if ($data->js_id!="")
                        <input type="hidden" name="js_id" value={{$data->js_id}}>
                    @else
                        <input type="hidden" name="js_id" value=>
                    @endif
                    <button type="submit" style="margin-left:19px;" class="btn btn-success">Cetak PDF</button> <code>untuk mencetak pdf, silahkan tekan tombol <b>Cari</b> terlebih dahulu kemudian tekan tombol <b>Cetak PDF</b></code>
                </form><br><br>
                <div style="text-align: center">
                    <table class="table-bordereless" style="width: 80%">
                        <tr>
                            <td rowspan="5" style="width: 30%; text-align:right"><img src="{{ asset('app-assets/images/ico/logo-rs.png') }}" class="img-fluid"style="width: 100px;"></td>
                            <td style="font-size: 24px;"><b>RUMAH SAKIT</b></td>
                        </tr>
                        <tr>
                            <td style="font-size: 20px;"><b>BHAYANGKARA DENPASAR</b></td>
                        </tr>
                        <tr>
                            <td>Jln. Trijata No. 32 Denpasar - Bali</td>
                        </tr>
                        <tr>
                            <td>Telp. (0361) 222908 - 234670; Fax. (0361) 238235; Customer Service 081236236838</td>
                        </tr>
                        <tr>
                            <td>Website: eswab.rstrijata.com, Email: rstrijata@gmail.com</td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th data-priority="1">No.</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Jenis Kelamin</th>
                                <th>Tanggal Lahir</th>
                                <th>Usia</th>
                                <th>Nomor HP</th>
                                <th>Alamat</th>
                                <th>Faskes</th>
                                <th>Tanggal Kirim</th>
                                <th>Tanggal Terima</th>
                                <th>Tanggal Periksa</th>
                                <th>Hasil</th>
                                <th>Satker</th>
                                <th>Subkategori Pasien</th>
                                <th>Jenis Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach ($hasil_swab as $hs)
                                <tr>
                                    <td style="text-align: center">{{$i}}</td>
                                    <td>{{$hs->hs_nama}}</td>
                                    <td>{{$hs->hs_nik}}</td>
                                    <td>{{$hs->hs_jk}}</td>
                                    <td>{{date("d-m-Y",strtotime($hs->hs_tgl_lahir))}}</td>
                                    <td>{{$hs->hs_usia}}</td>
                                    <td>{{$hs->hs_no_hp}}</td>
                                    <td>{{$hs->hs_alamat}}</td>
                                    <td>{{$hs->hs_faskes}}</td>
                                    @if (date("d-m-Y",strtotime($hs->hs_tgl_kirim_spesimen))=="01-01-1970")
                                        <td></td>
                                    @else
                                        <td>{{date("d-m-Y",strtotime($hs->hs_tgl_kirim_spesimen))}}</td>
                                    @endif
                                    @if (date("d-m-Y",strtotime($hs->hs_tgl_terima_spesimen))=="01-01-1970")
                                        <td></td>
                                    @else
                                        <td>{{date("d-m-Y",strtotime($hs->hs_tgl_terima_spesimen))}}</td>
                                    @endif
                                    @if (date("d-m-Y",strtotime($hs->hs_tgl_periksa_spesimen))=="01-01-1970")
                                        <td></td>
                                    @else
                                        <td>{{date("d-m-Y",strtotime($hs->hs_tgl_periksa_spesimen))}}</td>
                                    @endif
                                    @if ($hs->hs_hasil=="positif")
                                        <td style="color: #ff0000">{{ucwords($hs->hs_hasil)}}</td>
                                    @else
                                    <td style="color: #008000">{{ucwords($hs->hs_hasil)}}</td>
                                    @endif
                                    <td>{{$hs->hs_satker}}</td>
                                    <td>{{$hs->skp_nama}}</td>
                                    @if ($hs->js_id=="1")
                                        <td >Swab PCR</td>
                                    @else
                                        <td >Swab Antigen</td>
                                    @endif
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
    <script>
		$(document).ready(function() {
			$('.flatpickr-basic').flatpickr();
        });
        
    </script>
@endsection