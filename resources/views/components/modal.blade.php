<div class="modal fade {{ $type == 'slide' ? 'modal-slide-in' : '' }}" id="{{ $id }}" tabindex="-1">
    <div class="modal-dialog {{ $class }}">
        <div class="modal-content {{ $type == 'slide' ? 'pt-0' : '' }}">
            @if ($type == 'slide')
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                <div class="modal-header mb-1">
                    <h5 class="modal-title">{{ $title }}</h5>
                </div>
            @else
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">{{ $title }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            {{ $slot }}
        </div>
    </div>
</div>