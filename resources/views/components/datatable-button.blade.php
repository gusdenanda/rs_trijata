<div class="card">
    <div id="tabel_judul" hidden>{{ $title }}</div>
    <div id="id" hidden>{{ $id }}</div>
    <div id="button_title" hidden>{{ $buttonTitle }}</div>
    {{ $slot }}
</div>