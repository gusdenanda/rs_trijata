<div class="form-group">
    <label class="form-label">{{ $title }}</label>
    {{ $slot }}
</div>