@extends('layout.layout')

@section('title', 'Dashboard')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
    <section id="dashboard-ecommerce">
        <div class="row match-height">
            <!-- Medal Card -->
            <div class="col-xl-4 col-md-6 col-12">
                <div class="card card-congratulation-medal">
                    <div class="card-body">
                        <h5>Sistem Informasi Manajemen Hasil SWAB Test</h5> <br>
                        <h5>Rumah Sakit Bhayangkara Denpasar</h5> <br>
                        <p class="card-text font-small-3">Selamat Datang {{ Auth::user()->name }}</p>
                        
                        <img src="{{asset('app-assets/images/illustration/badge.svg')}}" class="congratulation-medal" alt="Medal Pic" />
                    </div>
                </div>
            </div>
            <!--/ Medal Card -->

            <!-- Statistics Card -->
            <div class="col-xl-8 col-md-6 col-12">
                <div class="card card-statistics">
                    <div class="card-header">
                        <h4 class="card-title">Statistics</h4>
                        <div class="d-flex align-items-center">
                            <p class="card-text font-small-2 mr-25 mb-0">Updated Recently</p>
                        </div>
                    </div>
                    <div class="card-body statistics-body">
                        <div class="row">
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="media">
                                    <div class="avatar bg-light-primary mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="trending-up" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$total_swab}}</h4>
                                        <p class="card-text font-small-3 mb-0">Total Swab Test</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                <div class="media">
                                    <div class="avatar bg-light-danger mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="arrow-up" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$total_swab_positif}}</h4>
                                        <p class="card-text font-small-3 mb-0">Jumlah Positif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12">
                                <div class="media">
                                    <div class="avatar bg-light-success mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="check-circle" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$total_swab_negatif}}</h4>
                                        <p class="card-text font-small-3 mb-0">Jumlah Negatif</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                <div class="media">
                                    <div class="avatar bg-light-info mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="user" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$total_user}}</h4>
                                        <p class="card-text font-small-3 mb-0">Jumlah Pengguna Sistem</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Statistics Card -->
        </div>
        <div class="row match-height">
            <!-- Company Table Card -->
            <div class="col-lg-4 col-12">
                <div class="card card-company-table">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Kategori Pasien</th>
                                        <th class="text-center">Negatif</th>
                                        <th class="text-center">Positif</th>
                                        <th class="text-center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $k = 0;
                                        $arr_nama = array();
                                        $arr_jumlah = array();
                                        $arr_hasil = array();
                                        foreach ($total_kategori_pasien as $tkp) {
                                            $arr_nama[$k] = $tkp->skp_nama;
                                            $arr_jumlah[$k] = $tkp->count;
                                            $arr_hasil[$k] = $tkp->hs_hasil;
                                            $k++;
                                        }
                                        $totalincrement = count($total_kategori_pasien);
                                        //echo $k;
                                    @endphp
                                    @for ($i = 0; $i < ($k-1); $i++)
                                        @php
                                            $j = $i % 3;
                                            $p = $i + 1 ;
                                            $total = (int)$arr_jumlah[$p] + (int)$arr_jumlah[$i];
                                        @endphp
                                        @if ($arr_nama[$i]==$arr_nama[$p])
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar {{$array_icon[$j]}} mr-1">
                                                            <div class="avatar-content">
                                                                <i data-feather="command" class="font-medium-3"></i>
                                                            </div>
                                                        </div>
                                                        <span><a href="javascript:void(0)" onclick="viewDetail('{{$arr_nama[$i]}}')">{{$arr_nama[$i]}}</a></span>
                                                    </div>
                                                </td>
                                                <td class="text-center">{{$arr_jumlah[$i]}} </td>
                                                <td class="text-center">{{$arr_jumlah[$p]}} </td>
                                                <td class="text-center">{{$total}} </td>
                                            </tr>
                                            @php
                                                $i++
                                            @endphp
                                        @else
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar {{$array_icon[$j]}} mr-1">
                                                            <div class="avatar-content">
                                                                <i data-feather="command" class="font-medium-3"></i>
                                                            </div>
                                                        </div>
                                                        <span><a href="javascript:void(0)" onclick="viewDetail('{{$arr_nama[$i]}}')">{{$arr_nama[$i]}}</a></span>
                                                    </div>
                                                </td>
                                                @if ($arr_hasil[$i]=="negatif")
                                                    <td class="text-center">{{$arr_jumlah[$i]}} </td>
                                                    <td class="text-center">0</td>
                                                @else
                                                    <td class="text-center">0</td>
                                                    <td class="text-center">{{$tkp->count}} </td>
                                                @endif
                                                <td class="text-center">{{$arr_jumlah[$i]}} </td>
                                            </tr>
                                        @endif                                        
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Company Table Card -->
            <!-- Revenue Report Card -->
            <div class="col-lg-8 col-12">
                <div class="card card-revenue-budget">
                    <div class="row mx-0">
                        <div class="col-md-8 col-12 revenue-report-wrapper">
                            <div class="d-sm-flex justify-content-between align-items-center mb-3" style="margin-top:10px;">
                                <h4 class="card-title mb-50 mb-sm-0">Gradik Hasil Swab Test Per Bulan</h4>
                                <div class="d-flex align-items-center">
                                    <div class="d-flex align-items-center mr-2">
                                        <span class="bullet bullet-primary font-small-3 mr-50 cursor-pointer"></span>
                                        <span>Positif</span>
                                    </div>
                                    <div class="d-flex align-items-center ml-75">
                                        <span class="bullet bullet-warning font-small-3 mr-50 cursor-pointer"></span>
                                        <span>Negatif</span>
                                    </div>
                                </div>
                            </div>
                            <div id="revenue-report-chart"></div>
                        </div>
                        <div class="col-md-4 col-12 budget-wrapper">
                            <h2 class="mb-25">Persentase Hasil SWAB Test</h2> <br>
                            <div id="earnings-chart"></div>
                            <div id="budget-chart" style="min-height: 80px;"><div id="apexcharts1u4nq7s6" class="apexcharts-canvas apexcharts1u4nq7s6 apexcharts-theme-light" 
                                style="width: 216px; height: 80px;"><svg id="SvgjsSvg1224" width="216" height="80" xmlns="http://www.w3.org/2000/svg" version="1.1" 
                                xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" 
                                transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG1226" class="apexcharts-inner apexcharts-graphical" 
                                transform="translate(0, 0)"><defs id="SvgjsDefs1225"><clipPath id="gridRectMask1u4nq7s6"><rect id="SvgjsRect1231" width="222" 
                                    height="82" x="-3" y="-1" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                </clipPath><clipPath id="gridRectMarkerMask1u4nq7s6"><rect id="SvgjsRect1232" width="220" height="84" x="-2" y="-2" rx="0" ry="0" 
                                    opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath></defs><line id="SvgjsLine1230"
                                     x1="0" y1="0" x2="0" y2="80" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1"
                                      height="80" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG1242" class="apexcharts-xaxis" 
                                      transform="translate(0, 0)"><g id="SvgjsG1243" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"></g></g><g id="SvgjsG1245" 
                                      class="apexcharts-grid"><g id="SvgjsG1246" class="apexcharts-gridlines-horizontal" style="display: none;"><line id="SvgjsLine1248" x1="0"
                                         y1="0" x2="216" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1249" x1="0" y1="20" 
                                         x2="216" y2="20" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1250" x1="0" y1="40" x2="216" 
                                         y2="40" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1251" x1="0" y1="60" x2="216" y2="60" stroke="#e0e0e0" 
                                         stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine1252" x1="0" y1="80" x2="216" y2="80" stroke="#e0e0e0" stroke-dasharray="0" 
                                         class="apexcharts-gridline"></line></g><g id="SvgjsG1247" class="apexcharts-gridlines-vertical" style="display: none;"></g><line 
                                         id="SvgjsLine1254" x1="0" y1="80" x2="216" y2="80" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine1253" x1="0" y1="1" x2="0" 
                                         y2="80" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG1233" class="apexcharts-line-series apexcharts-plot-series">
                                             <g id="SvgjsG1234" class="apexcharts-series" seriesName="seriesx1" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath1237" 
                                                d="M0 19C7.559999999999999 19 14.040000000000003 32 21.6 32C29.16 32 35.64 11 43.2 11C50.76 11 57.24 28 64.8 28C72.36 28 78.84 20 86.4 20C93.96000000000001 20 100.44 40 108 40C115.56 40 122.03999999999999 1 129.6 1C137.16 1 143.64000000000001 20 151.20000000000002 20C158.76000000000002 20 165.24 21 172.8 21C180.36 21 186.84 37 194.4 37C201.96 37 208.44 18 216 18C216 18 216 18 216 18 " 
                                                fill="none" fill-opacity="1" stroke="rgba(115,103,240,0.85)" stroke-opacity="1" stroke-linecap="butt" stroke-width="2" stroke-dasharray="0" 
                                                class="apexcharts-line" index="0" clip-path="url(#gridRectMask1u4nq7s6)" pathTo="M 0 19C 7.56 19 14.040000000000003 32 21.6 32C 29.16 32 35.64 11 43.2 11C 50.76 11 57.24 28 64.8 28C 72.36 28 78.84 20 86.4 20C 93.96000000000001 20 100.44 40 108 40C 115.56 40 122.03999999999999 1 129.6 1C 137.16 1 143.64000000000001 20 151.20000000000002 20C 158.76000000000002 20 165.24 21 172.8 21C 180.36 21 186.84 37 194.4 37C 201.96 37 208.44 18 216 18" 
                                                pathFrom="M -1 80L -1 80L 21.6 80L 43.2 80L 64.8 80L 86.4 80L 108 80L 129.6 80L 151.20000000000002 80L 172.8 80L 194.4 80L 216 80"></path>
                                                <g id="SvgjsG1235" class="apexcharts-series-markers-wrap" data:realIndex="0"></g></g><g id="SvgjsG1238" class="apexcharts-series" seriesName="seriesx2" 
                                                data:longestSeries="true" rel="2" data:realIndex="1"><path id="SvgjsPath1241" d="M0 60C7.559999999999999 60 14.040000000000003 70 21.6 70C29.16 70 35.64 50 43.2 50C50.76 50 57.24 65 64.8 65C72.36 65 78.84 57 86.4 57C93.96000000000001 57 100.44 80 108 80C115.56 80 122.03999999999999 55 129.6 55C137.16 55 143.64000000000001 65 151.20000000000002 65C158.76000000000002 65 165.24 60 172.8 60C180.36 60 186.84 75 194.4 75C201.96 75 208.44 53 216 53C216 53 216 53 216 53 " 
                                                fill="none" fill-opacity="1" stroke="rgba(220,218,227,0.85)" stroke-opacity="1" stroke-linecap="butt" stroke-width="1" stroke-dasharray="5" 
                                                class="apexcharts-line" index="1" clip-path="url(#gridRectMask1u4nq7s6)" pathTo="M 0 60C 7.56 60 14.040000000000003 70 21.6 70C 29.16 70 35.64 50 43.2 50C 50.76 50 57.24 65 64.8 65C 72.36 65 78.84 57 86.4 57C 93.96000000000001 57 100.44 80 108 80C 115.56 80 122.03999999999999 55 129.6 55C 137.16 55 143.64000000000001 65 151.20000000000002 65C 158.76000000000002 65 165.24 60 172.8 60C 180.36 60 186.84 75 194.4 75C 201.96 75 208.44 53 216 53" 
                                                pathFrom="M -1 80L -1 80L 21.6 80L 43.2 80L 64.8 80L 86.4 80L 108 80L 129.6 80L 151.20000000000002 80L 172.8 80L 194.4 80L 216 80"></path>
                                                <g id="SvgjsG1239" class="apexcharts-series-markers-wrap" data:realIndex="1"></g></g><g id="SvgjsG1236" class="apexcharts-datalabels" data:realIndex="0"></g>
                                                <g id="SvgjsG1240" class="apexcharts-datalabels" data:realIndex="1"></g></g><line id="SvgjsLine1255" x1="0" y1="0" x2="216" y2="0" stroke="#b6b6b6" 
                                                stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine1256" x1="0" y1="0" x2="216" y2="0" stroke-dasharray="0" 
                                                stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG1257" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG1258" 
                                                class="apexcharts-xaxis-annotations"></g><g id="SvgjsG1259" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect1229" width="0" height="0" 
                                                x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG1244" class="apexcharts-yaxis" 
                                                rel="0" transform="translate(-18, 0)"></g><g id="SvgjsG1227" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 40px;"></div></div></div>
                            RS. Bhayangkara Denpasar
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Revenue Report Card -->
        </div>
    </section>
    <x-modal title="" type="normal" class="modal-lg" id="modal">
        <form name="form" id="form" method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <h4 id="subkategori"></h4>
                <table class="table" id="tableSatker">
                    <thead>
                        <tr>
                            <th>Satker</th>
                            <th class="text-center">Negatif</th>
                            <th class="text-center">Positif</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Tutup</button>
            </div>
        </form>
    </x-modal>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <!-- END: Page Vendor JS-->
	<script>
		var link = null;
	</script>
    
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
    <script>
        $(window).on('load', function () {
            var $barColor = '#f3f3f3';
            var $trackBgColor = '#EBEBEB';
            var $textMutedColor = '#b9b9c3';
            var $budgetStrokeColor2 = '#dcdae3';
            var $goalStrokeColor2 = '#51e5a8';
            var $strokeColor = '#ebe9f1';
            var $textHeadingColor = '#5e5873';
            var $earningsStrokeColor2 = '#28c76f66';
            var $earningsStrokeColor3 = '#28c76f33';

            var $revenueReportChart = document.querySelector('#revenue-report-chart');
            var revenueReportChartOptions;
            var revenueReportChart;

            var $earningsChart = document.querySelector('#earnings-chart');
            var earningsChartOptions;
            var earningsChart;

            //------------ Revenue Report Chart ------------
            //----------------------------------------------
            revenueReportChartOptions = {
                chart: {
                height: 330,
                stacked: true,
                type: 'bar',
                toolbar: { show: false }
                },
                plotOptions: {
                bar: {
                    columnWidth: '17%',
                    endingShape: 'rounded'
                },
                distributed: true
                },
                colors: [window.colors.solid.primary, window.colors.solid.warning],
                series: [
                {
                    name: 'Positif',
                    data: [@foreach($blnpos as $pos)
                                {{$pos["count"]}},
                            @endforeach]
                },
                {
                    name: 'Negatif',
                    data: [@foreach($blnneg as $neg)
                                {{$neg["count"]}},
                            @endforeach]
                }
                ],
                dataLabels: {
                enabled: false
                },
                legend: {
                show: false
                },
                grid: {
                padding: {
                    top: -20,
                    bottom: -10
                },
                yaxis: {
                    lines: { show: false }
                }
                },
                xaxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                labels: {
                    style: {
                    colors: $textMutedColor,
                    fontSize: '0.86rem'
                    }
                },
                axisTicks: {
                    show: false
                },
                axisBorder: {
                    show: false
                }
                },
                yaxis: {
                labels: {
                    style: {
                    colors: $textMutedColor,
                    fontSize: '0.86rem'
                    }
                }
                }
            };
            revenueReportChart = new ApexCharts($revenueReportChart, revenueReportChartOptions);
            revenueReportChart.render();

            //--------------- Earnings Chart ---------------
            //----------------------------------------------
            earningsChartOptions = {
                chart: {
                type: 'donut',
                height: 180,
                toolbar: {
                    show: false
                }
                },
                dataLabels: {
                enabled: false
                },
                series: [{{$persentase_positif}}, {{$persentase_negatif}}],
                legend: { show: false },
                comparedResult: [2, -3, 8],
                labels: ['Positif', 'Negatif'],
                stroke: { width: 0 },
                colors: [window.colors.solid.primary, window.colors.solid.warning ],
                grid: {
                padding: {
                    right: -20,
                    bottom: -8,
                    left: -20
                }
                },
                plotOptions: {
                pie: {
                    startAngle: -10,
                    donut: {
                    labels: {
                        show: true,
                        name: {
                        offsetY: 15
                        },
                        value: {
                        offsetY: -15,
                        formatter: function (val) {
                            return parseInt(val) + '%';
                        }
                        },
                        total: {
                        show: true,
                        offsetY: 15,
                        label: 'Positif',
                        formatter: function (w) {
                            return {{$persentase_positif}} + '%';
                        }
                        }
                    }
                    }
                }
                },
                responsive: [
                {
                    breakpoint: 1325,
                    options: {
                    chart: {
                        height: 100
                    }
                    }
                },
                {
                    breakpoint: 1200,
                    options: {
                    chart: {
                        height: 120
                    }
                    }
                },
                {
                    breakpoint: 1045,
                    options: {
                    chart: {
                        height: 100
                    }
                    }
                },
                {
                    breakpoint: 992,
                    options: {
                    chart: {
                        height: 120
                    }
                    }
                }
                ]
            };
            earningsChart = new ApexCharts($earningsChart, earningsChartOptions);
            earningsChart.render();

            $('#modal').on('hidden.bs.modal', function (e) {
                $("#tableSatker").empty();
                $('#tableSatker').append('<tr bgcolor="#F3F2F7"><th>Satker</th><th class="text-center">Negatif</th><th class="text-center">Positif</th><th class="text-center">Total</th></tr>');
			});
        });
        function viewDetail(id){
            var array_icon = ["bg-light-primary","bg-light-warning","bg-light-success"];
            $.get( "{{ url('dashboard/reportpersubkategori') }}/"+ id, function( data ) {
				var d = JSON.parse(data);
                var ba = d.length - 1;
                console.log(d);
                $('.modal-title').text('Rekap Hasil Swab Test Per Satker Berdasarkan Kategori '+id);
                for (i = 0; i < d.length; i++){
                    //console.log(d.count);
                    var j = i % 3;
                    var p = i + 1;
                    if(d.length>1 && i<ba){
                        var total = d[p].count + d[i].count;
                        if (d[i].hs_satker==d[p].hs_satker){
                            $('#tableSatker').append('<tr><td><div class="d-flex align-items-center"><div class="avatar '+array_icon[j]+' mr-1"><div class="avatar-content"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command font-medium-3"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg></div></div><span>'+d[i].hs_satker+'</span></div></td><td style="text-align:center">'+d[i].count+' </td><td style="text-align:center">'+d[p].count+' </td><td style="text-align:center">'+total+' </td></tr>');
                            i++;
                        }else{
                            if(d[i].hs_hasil='negatif'){
                                $('#tableSatker').append('<tr><td><div class="d-flex align-items-center"><div class="avatar '+array_icon[j]+' mr-1"><div class="avatar-content"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command font-medium-3"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg></div></div><span>'+d[i].hs_satker+'</span></div></td><td style="text-align:center">'+d[i].count+' </td><td style="text-align:center">0 </td><td style="text-align:center">'+d[i].count+' </td></tr>');
                            }else{
                                $('#tableSatker').append('<tr><td><div class="d-flex align-items-center"><div class="avatar '+array_icon[j]+' mr-1"><div class="avatar-content"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command font-medium-3"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg></div></div><span>'+d[i].hs_satker+'</span></div></td><td style="text-align:center">0</td><td style="text-align:center">'+d[i].count+'  </td><td style="text-align:center">'+d[i].count+' </td></tr>');
                            }
                        }                 
                    }else{
                        if(d[i].hs_hasil='negatif'){
                                $('#tableSatker').append('<tr><td><div class="d-flex align-items-center"><div class="avatar '+array_icon[j]+' mr-1"><div class="avatar-content"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command font-medium-3"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg></div></div><span>'+d[i].hs_satker+'</span></div></td><td style="text-align:center">'+d[i].count+' </td><td style="text-align:center">0 </td><td style="text-align:center">'+d[i].count+' </td></tr>');
                            }else{
                                $('#tableSatker').append('<tr><td><div class="d-flex align-items-center"><div class="avatar '+array_icon[j]+' mr-1"><div class="avatar-content"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-command font-medium-3"><path d="M18 3a3 3 0 0 0-3 3v12a3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3H6a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3V6a3 3 0 0 0-3-3 3 3 0 0 0-3 3 3 3 0 0 0 3 3h12a3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path></svg></div></div><span>'+d[i].hs_satker+'</span></div></td><td style="text-align:center">0</td><td style="text-align:center">'+d[i].count+'  </td><td style="text-align:center">'+d[i].count+' </td></tr>');
                            }
                    }
                    
                }
				
			});
            $('#modal').modal('show');
        }
    </script>
	
@endsection