<!DOCTYPE html>
<html>
<head>
    <title>Hasil SWAB {{$hs_nama}}</title>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 60px;
            header: page-header;
	        footer: page-footer;
        }
        header {
            position: fixed;
            top: -100px;
            left: -65px;
            right: -65px;
            height: 70px;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed; 
            bottom: -60px; 
            left: -65px; 
            right: -65px;
            height: 90px; 

            /** Extra personal styles **/
            text-align: center;
            line-height: 35px;
        }
        .tableborder {
            border-left: 0.01em solid #000;
            border-right: 0;
            border-top: 0.01em solid #000;
            border-bottom: 0;
            border-collapse: collapse;
        }
        .table {
            border-collapse: collapse;
        }
        .tabletdborder{
            border-left: 0;
            border-right: 0.01em solid #000;
            border-top: 0;
            border-bottom: 0.01em solid #000;
            padding: 5px;
        }
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_atas.png") }}" alt="" style="width: 120%; margin:-40px -65px 0px -65px; ;">
    </htmlpageheader>
    <main>
        <br><br><br>
        @if ($js_id=="2")
        <p style="text-align: center; font-size:20px; line-height: 10px; font-weight:bold;"><u>LABORATORY EXAMINATION RESULTS</u></p>         
        @else
            <p style="text-align: center; font-size:20px; line-height: 10px; font-weight:bold;"><u>EXAMINATION RESULT FOR COVID-19</u></p>
            <p style="text-align: center; font-size:20px;  line-height: 10px; font-weight:bold;"><i>Real-Time PCR Method<i></p>
        @endif
        <br>
        <table style="width:100%">
            <tr>
                <td>
                    <table style="width:100%; border:none">
                        @if ($js_id!="1")
                            <tr>
                                <td>Sample ID</td>
                                <td>:</td>
                                <td>{{$hs_nomor_spesimen_antigen}}</td>
                            </tr>
                            <tr>
                                <td>Check Date</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_periksa_spesimen))}}</td>
                            </tr>
                        @endif
                        <tr>
                            <td style="width:30%; border:none;">Name</td>
                            <td style="width:3%; border:none;">:</td>
                            <td style="border:none;">{{$hs_nama}}</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>:</td>
                            <td>{{date("d/m/Y",strtotime($hs_tgl_lahir))}}</td>
                        </tr>
                        <tr>
                            <td>Age / Gender</td>
                            <td>:</td>
                            <td>{{$hs_usia}} Thn / {{ucwords($hs_jk)}}</td>
                        </tr>
                        @if ($js_id=="1")
                            <tr>
                                <td valign="top">Address</td>
                                <td valign="top">:</td>
                                <td>{{$hs_alamat}}</td>
                            </tr>
                            <tr>
                                <td>Handphone</td>
                                <td>:</td>
                                <td>{{$hs_no_hp}}</td>
                            </tr>
                            <tr>
                                <td>Date of specimens</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_kirim_spesimen))}}</td>
                            </tr>
                            <tr>
                                <td>The date the specimen was completed</td>
                                <td>:</td>
                                <td>{{date("d/m/Y",strtotime($hs_tgl_periksa_spesimen))}}</td>
                            </tr>
                            <tr>
                                <td>Specimen to</td>
                                <td>:</td>
                                <td>{{$hs_sampel_ke}}</td>
                            </tr>
                            <tr>
                                <td>Result</td>
                                <td>:</td>
                                @if (strtolower($hs_hasil)=="positif")
                                    <td style="color:#FF0000;">Positive</td>
                                @else
                                    <td>Negative</td>
                                @endif
                            </tr>
                            @if ($hs_ct_value!="")
                                <tr>
                                    <td>CT Value</td>
                                    <td>:</td>
                                    @if ($hs_ct_value > $ct_value)
                                        <td>Not Detected (Cut off ≤ {{$ct_value}})</td>
                                    @else
                                        <td>{{$hs_ct_value}} (Cut off ≤ {{$ct_value}})</td>
                                    @endif
                                </tr>
                            @endif
                        @endif
                    </table>
                </td>
            </tr>
        </table>
        <br><br>
        @if ($js_id=="2")
            <table style="width:100%; font-size:18px;" class="tableborder">
                <tr>
                    <td style="width: 40%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">CHECKING TYPE</td>
                    <td style="width: 30%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">RESULT</td>
                    <td style="text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">NORMAL VALUE</td>
                </tr>
                <tr>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Covid-19 Antigen Rapid Test</td>
                    @if (strtolower($hs_hasil)=="positif")
                        <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">Positive</td>
                    @elseif(strtolower($hs_hasil)=="negatif")
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negative</td>
                    @else
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @endif
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negatif</td>
                </tr>
            </table><br><br>
            <table style="width:100%" class="tableborder">
                <tr>
                    <td style="width:45%; text-align:justify" valign="top" class="tabletdborder">
                       @if (strtolower($hs_hasil)=="negatif")
                           <table style="width:100%; font-size:12px;" class="tableborder" style="border-left: 0;
                                    border-right: 0;
                                    border-top: 0.01em solid #ffffff;
                                    border-bottom: 0;
                                    border-collapse: collapse;">
                                <tr>
                                    <td style="width:7%" valign="top">1.</td>
                                    <td>Negative results do not rule out the possibility of being infected with SARS-CoV-2 so there is still 
                                        a risk of transmitting it to others, it is recommended to retest or confirmatory tests with NAAT, 
                                        if the pretest probability is relatively high, especially if the patient is symptomatic or is known 
                                        to have contact with a confirmed Covid-19 person;</td>
                                </tr>
                                <tr>
                                    <td> 2.</td>
                                    <td>Negative results can occur when the quantity of antigen in the specimen is below the detection level of the instrument.</td>
                                </tr>
                            </table>
                       @else
                           <table style="width:100%; font-size:12px;" class="tableborder" style="border-left: 0;
                                border-right: 0;
                                border-top: 0.01em solid #ffffff;
                                border-bottom: 0;
                                border-collapse: collapse;">
                                <tr>
                                    <td style="width:7%" valign="top">1.</td>
                                    <td>Confirmatory examination with RT-PCR examination;</td>
                                </tr>
                                <tr>
                                    <td> 2.</td>
                                    <td>Perform quarantine or isolation according to the criteria;</td>
                                </tr>
                                <tr>
                                    <td> 3.</td>
                                    <td>Implementing Clean and Healthy Living Behavior, cough etiquette, using a mask when sick, maintaining stamina and physical distancing.</td>
                                </tr>
                            </table>
                       @endif
                    </td>
                    <td style="text-align:center; width:25%" valign="top" class="tabletdborder">
                        Checker<br><br>
                        @if ($pemeriksa_ttd!="")
                            <img src="{{ public_path("images/".$pemeriksa_ttd."") }}" alt="" style="width: 120px; height:80px; margin-left:-30px;"> <br>
                        @endif
                        <u>Analys</u>
                    </td>
                    <td style="text-align:center" valign="top" class="tabletdborder">
                        Doctor<br><br>
                        @if ($ps_ttd!="")
                            <img src="{{ public_path("images/".$ps_ttd."") }}" alt="" style="width: 400px; height:80px; z-index:-1; margin-top:-10px; margin-left:-90px; margin-right:10px;"> <br>
                        @endif
                        <u>{{$ps_nama}}</u><br>
                        {{$ps_pangkat}} NRP {{$ps_nrp}}
                    </td>
                </tr>
            </table>
        @else
            <!--<table style="width:100%; font-size:18px;" class="tableborder">
                <tr>
                    <td style="width: 30%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">CHECKING TYPE</td>
                    <td style="width: 25%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">RESULT</td>
                    <td style="width: 25%; text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">VALUE</td>
                    <td style="text-align:center;line-height: 28px;background:#ccc;font-weight:bold;" valign="center" class="tabletdborder">METHOD</td>
                </tr>
                <tr>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">SARS-CoV-2</td>
                    @if (strtolower($hs_hasil)=="positif")
                        <td style="text-align:center;line-height: 28px; color:#FF0000;" class="tabletdborder" valign="center">Positive</td>
                    @elseif(strtolower($hs_hasil)=="negatif")
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negative</td>
                    @else
                        <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">{{ucwords($hs_hasil)}}</td>
                    @endif
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">Negative</td>
                    <td style="text-align:center;line-height: 28px;" class="tabletdborder" valign="center">RT-PCR</td>
                </tr>
            </table><br><br><br><br>-->
            <table style="border:0; width:100%">
                <tr>
                    <td style="width:45%; text-align:center" valign="top">
                        Doctor<br><br>
                        @if ($ps_ttd!="")
                            <img src="{{ public_path("images/".$ps_ttd."") }}" alt="" style="width: 250px; height:80px"> <br>
                        @endif
                        <u>{{$ps_nama}}</u><br>
                        {{$ps_pangkat}} NRP {{$ps_nrp}}
                    </td>
                    <td style="width:15%" valign="top"></td>
                    <td style="text-align:center" valign="top">Checker<br><br>
                        @if ($pemeriksa_ttd!="")
                            <img src="{{ public_path("images/".$pemeriksa_ttd."") }}" alt="" style="width: 150px; height:80px"> <br>
                        @endif
                        <u>{{$pemeriksa_nama}} </u>
                    </td>
                </tr>
            </table>
        @endif
        <br><br>
        <table style="border:0; width:100%">
            <tr>
                <td style="text-align: right; width:14%"><img src="data:image/svg;base64, {!! $qrcode !!}"></td>
                <td style="font-size: 12px; width:50%">Please scan the QR-Code on the side to validate the document resulting from this Swab Test.</td>
                <td></td>
            </tr>
            @if ($js_id=="1")
                <tr>
                    <td colspan="3"></td>
                <tr>
                <tr>
                    <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Note :</td>
                <tr>
                <tr>
                    <td style="text-align: right;" valign="top">-</td>
                    <td style="font-size: 10px; " valign="top" colspan="2">The examination was carried out using the Realtime PCR method and only described the conditions at the time of specimen collection.</td>
                </tr>
                <tr>
                    <td style="text-align: right;" valign="top">-</td>
                    <td style="font-size: 10px; " valign="top" colspan="2">Operational Permit for PCR Laboratory of Bhayangkara Hospital Denpasar as a COVID-19 examination laboratory from the Ministry of Health of the Republic of Indonesia number SR.01.07/II/2738/2021.</td>
                </tr>
            @endif
        </table>
        
    </main>
    <htmlpagefooter name="page-footer">
        <img src="{{ public_path("app-assets/images/ico/kop_surat_bawah.png") }}" alt="" style="width: 100%;" style="width: 120%; margin:0px -65px -40px -65px; ;">
    </htmlpagefooter>

    
</body>
</html>