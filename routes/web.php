<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KategoriPasienController as KategoriPasienController;
use App\Http\Controllers\JenisSwabController as JenisSwabController;
use App\Http\Controllers\SettingController as SettingController;
use App\Http\Controllers\SubkategoriPasienController as SubkategoriPasienController;
use App\Http\Controllers\UserController as UserController;
use App\Http\Controllers\HasilSwabController as HasilSwabController;
use App\Http\Controllers\DashboardController as DashboardController;
use App\Http\Controllers\PenandatanganSwabController as PenandatanganSwabController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('search_hasil_swab');
});


//Route::view('home', 'home');

//kategori pasien
Route::resource('kategori-pasien', KategoriPasienController::class)->middleware('auth');
Route::get('data/kategori-pasien', [KategoriPasienController::class, 'data'])->name('kategori-pasien.data')->middleware('auth');

//jenis swab
Route::resource('jenis-swab', JenisSwabController::class)->middleware('auth');
Route::get('data/jenis-swab', [JenisSwabController::class, 'data'])->name('jenis-swab.data')->middleware('auth');

//setting
Route::resource('setting', SettingController::class)->middleware('auth');
Route::get('data/setting', [SettingController::class, 'data'])->name('setting.data')->middleware('auth');

//subkategori pasien
Route::resource('subkategori-pasien', SubkategoriPasienController::class)->middleware('auth');
Route::get('data/subkategori-pasien', [SubkategoriPasienController::class, 'data'])->name('subkategori-pasien.data')->middleware('auth');

//subkategori pasien
Route::resource('penandatangan', PenandatanganSwabController::class)->middleware('auth');
Route::get('data/penandatangan', [PenandatanganSwabController::class, 'data'])->name('penandatangan.data')->middleware('auth');

//User
Route::resource('user', UserController::class)->middleware('auth');
Route::get('data/user', [UserController::class, 'data'])->name('user.data')->middleware('auth');

//hasil swab
Route::post('/hasil-swab/import_excel', [HasilSwabController::class, 'import_excel'])->name('hasil-swab.import_excel')->middleware('auth');
Route::get('/hasil-swab/generatenomor', [HasilSwabController::class, 'generateNomor'])->name('hasil-swab.generatenomor')->middleware('auth');
Route::get('/hasil-swab/generate-pdf/{id}', [HasilSwabController::class, 'generatePDF'])->name('hasil-swab.generatepdf');
Route::get('/hasil-swab/generate-pdf-en/{id}', [HasilSwabController::class, 'generatePDFEn'])->name('hasil-swab.generatepdf-en');
Route::resource('hasil-swab', HasilSwabController::class)->middleware('auth');
Route::get('data/hasil-swab', [HasilSwabController::class, 'data'])->name('hasil-swab.data')->middleware('auth');

//cetakmultipple
Route::post('/hasil-swab/generate-pdf-multiple', [HasilSwabController::class, 'generatePDFMultiple'])->name('hasil-swab.generatepdfmultiple')->middleware('auth');

//report swab
Route::any('report/hasil-swab', [HasilSwabController::class, 'viewReport'])->name('report-hasil-swab.viewreport')->middleware('auth');
Route::get('report/hasil-swab/viewdata', [HasilSwabController::class, 'viewDataReport'])->name('report-hasil-swab.viewdatareport')->middleware('auth');
Route::post('report/hasil-swab/cetakpdf', [HasilSwabController::class, 'cetakPDF'])->name('report-hasil-swab.cetakpdf')->middleware('auth');

//id
Route::get('/cek-validation-swab/{id}', [HasilSwabController::class, 'cekValidation'])->name('cek-validation-swab.validation');
Route::get('hasil-swabtest', function () {
    return view('search_hasil_swab');
})->name('hasil-swab.formsearch');
Route::post('/hasil-swabtest/search', [HasilSwabController::class, 'searchHasilSwab'])->name('hasil-swab.search');

//en
Route::get('/cek-validation-swab/en/{id}', [HasilSwabController::class, 'cekValidationEn'])->name('cek-validation-swab.validation-en');
Route::get('swabtest-result', function () {
    return view('search_hasil_swab_en');
})->name('swabtest-result.formsearch');
Route::post('/swabtest-result/search', [HasilSwabController::class, 'searchHasilSwabEn'])->name('swabtest-result.search');

//dashboard
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard.index')->middleware('auth');
Route::get('dashboard/reportpersubkategori/{id}', [DashboardController::class, 'reportPersubkategori'])->name('dashboard.reportpersubkategori')->middleware('auth');