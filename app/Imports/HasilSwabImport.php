<?php

namespace App\Imports;

use App\Models\HasilSwab;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;



class HasilSwabImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    
    public function collection(Collection $rows)
    {
        $i = 0;
        $data = HasilSwab::where('js_id','=','2')
                            ->whereMonth('hs_tgl_periksa_spesimen', '=',date('m'))
                            ->whereYear('hs_tgl_periksa_spesimen', '=', date('Y'))
                            ->max('hs_nomor_spesimen_antigen');
        //return $data;
        if ( is_null($data) ) {
            $data=1;
        }else{
            if($data==""){
                $data=1;
            }else{
                $data= (int)$data + 1;
            }
            
        }
        foreach ($rows as $row) 
        {
            if($i>0 && $row[1]!=""){
                $nik = str_replace("'","",$row[2]);
                $code = Hash::make(($nik.date("U")));
                $code = str_replace("/","-",$code);
                $row[22] = str_replace(",",".",$row[22]);

                if($row[10]=="2"){
                    $row[11] = $data;
                }
                if($row[22]==""){
                    $row[22]=NULL;
                }
                if(!isset($row[13])){
                    $row[13]=NULL;
                }
                if(!isset($row[15])){
                    $row[15]=NULL;
                }
                if(!isset($row[17])){
                    $row[17]=NULL;
                }
                HasilSwab::create([
                    'hs_nama' => $row[1],
                    'hs_nik' => $nik, 
                    'hs_tgl_lahir' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[3])), 
                    'hs_usia' => $row[4], 
                    'hs_jk' => strtolower($row[5]), 
                    'hs_no_hp' => str_replace("'","",$row[6]),
                    'hs_alamat' => $row[7], 
                    'hs_faskes' => $row[8], 
                    'hs_sampel_ke' => $row[9], 
                    'js_id' => $row[10], 
                    'hs_nomor_spesimen' => $row[11], 
                    'hs_tgl_kirim_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[12])), 
                    'hs_jam_kirim_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[13])), 
                    'hs_tgl_terima_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[14])), 
                    'hs_jam_terima_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[15])), 
                    'hs_tgl_periksa_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[16])), 
                    'hs_jam_periksa_spesimen' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[17])), 
                    'hs_hasil' => strtolower($row[18]), 
                    'hs_kriteria' => $row[19], 
                    'hs_satker' => strtoupper($row[20]), 
                    'skp_id' => $row[21], 
                    'hs_ct_value' => $row[22], 
                    'hs_kode_lab' => $row[23], 
                    'hs_diperiksa_oleh' => $row[24], 
                    'hs_code' => $code, 
                ]);
            }
            $data ++;
            $i++;
        }
    }
}
