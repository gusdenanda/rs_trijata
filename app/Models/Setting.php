<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'setting';

    static function storeSetting($request)
    {
        Setting::create([
            'ct_value' => $request->ct_value
        ]);
    }

    static function updateSetting($id, $request)
    {
        Setting::where('id', $id)->update([
            'ct_value' => $request->ct_value
        ]);
    }

    static function deleteSetting($id)
    {
        Setting::where('id', $id)->delete();
    }
}
