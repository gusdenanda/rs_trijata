<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisSwab extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'jenis_swab';

    static function storeJenisSwab($request)
    {
        JenisSwab::create([
            'js_nama' => $request->js_nama,
            'js_active'  => $request->js_active
        ]);
    }

    static function updateJenisSwab($id, $request)
    {
        JenisSwab::where('id', $id)->update([
            'js_nama' => $request->js_nama,
            'js_active'  => $request->js_active
        ]);
    }

    static function deleteJenisSwab($id)
    {
        JenisSwab::where('id', $id)->delete();
    }
}
