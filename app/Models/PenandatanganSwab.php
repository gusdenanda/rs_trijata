<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenandatanganSwab extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'penandatangan_swab';

    static function storePenandatanganSwab($request)
    {
        PenandatanganSwab::create([
            'ps_nama' => $request->ps_nama,
            'ps_nrp' => $request->ps_nrp,
            'ps_pangkat' => $request->ps_pangkat,
            'ps_penandatangan' => $request->ps_penandatangan,
            'ps_active'  => $request->ps_active
        ]);
    }
}
