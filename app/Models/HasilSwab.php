<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilSwab extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'hasil_swab';

    static function updateHasilSwab($id, $request)
    {
        HasilSwab::whereId($id)->update([
            'hs_nama'      => $request->hs_nama,
            'hs_nik'     => $request->hs_nik,
            'hs_tgl_lahir'     => $request->hs_tgl_lahir,
            'hs_usia'     => $request->hs_usia,
            'hs_jk'     => $request->hs_jk,
            'hs_no_hp'     => $request->hs_no_hp,
            'hs_alamat'     => $request->hs_alamat,
            'hs_faskes'     => $request->hs_faskes,
            'hs_sampel_ke'     => $request->hs_sampel_ke,
            'hs_jenis_spesimen'     => $request->hs_jenis_spesimen,
            'hs_nomor_spesimen'     => $request->hs_nomor_spesimen,
            'hs_tgl_kirim_spesimen'     => $request->hs_tgl_kirim_spesimen,
            'hs_tgl_terima_spesimen'     => $request->hs_tgl_terima_spesimen,
            'hs_tgl_periksa_spesimen'     => $request->hs_tgl_periksa_spesimen,
            'hs_hasil'     => $request->hs_hasil,
            'hs_kriteria'     => $request->hs_kriteria,
            'hs_satker'     => $request->hs_satker,
            'skp_id'     => $request->skp_id
        ]);    
    }

    static function deleteHasilSwab($id)
    {
        HasilSwab::whereId($id)->delete();
    }
}
