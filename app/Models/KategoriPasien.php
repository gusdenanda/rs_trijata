<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriPasien extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'kategori_pasien';

    static function storeKategoriPasien($request)
    {
        KategoriPasien::create([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
    }

    static function updateKategoriPasien($id, $request)
    {
        KategoriPasien::where('id', $id)->update([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
    }

    static function deleteKategoriPasien($id)
    {
        KategoriPasien::where('id', $id)->delete();
    }
}
