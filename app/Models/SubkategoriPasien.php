<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubkategoriPasien extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = ['id','created_at','updated_at'];
    protected $table = 'subkategori_pasien';

    static function storeSubkategoriPasien($request)
    {
        SubkategoriPasien::create([
            'skp_nama' => $request->skp_nama,
            'skp_active'  => $request->skp_active,
            'kp_id'  => $request->kp_id
        ]);
    }
}
