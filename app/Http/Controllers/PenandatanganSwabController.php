<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PenandatanganSwab;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class PenandatanganSwabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('penandatangan_swab');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ttd'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $ttd = "";
        if ($request->ttd != NULL) {
            $image = $request->file('ttd');
            $ttd = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('repositories/public/images', $ttd);
        }
        PenandatanganSwab::create([
            'ps_nama' => $request->ps_nama,
            'ps_nrp' => $request->ps_nrp,
            'ps_pangkat' => $request->ps_pangkat,
            'ps_penandatangan' => $request->ps_penandatangan,
            'ps_ttd' => $ttd,
            'ps_active'  => $request->ps_active
        ]);
        
        return $this->status(1, 'Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PenandatanganSwab::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'ttd'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        $ttd = "";
        if ($request->ttd != NULL) {
            $image = $request->file('ttd');
            $ttd = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('repositories/public/images', $ttd);
            PenandatanganSwab::where('id', $id)->update([
                'ps_nama' => $request->ps_nama,
                'ps_nrp' => $request->ps_nrp,
                'ps_pangkat' => $request->ps_pangkat,
                'ps_penandatangan' => $request->ps_penandatangan,
                'ps_ttd' => $ttd,
                'ps_active'  => $request->ps_active
            ]);
        }else{
            PenandatanganSwab::where('id', $id)->update([
                'ps_nama' => $request->ps_nama,
                'ps_nrp' => $request->ps_nrp,
                'ps_pangkat' => $request->ps_pangkat,
                'ps_penandatangan' => $request->ps_penandatangan,
                'ps_active'  => $request->ps_active
            ]);
        }

        
        
        return $this->status(1, 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PenandatanganSwab::where('id', $id)->delete();
        
        return $this->status(1, 'Data berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $data = PenandatanganSwab::select(['id', 'ps_nama','ps_nrp','ps_pangkat', 'ps_active']);
        
        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                return '<form action="penandatangan/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->kp_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->addColumn('ps_status', function ($item) {
                if($item->ps_active=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Aktif</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','ps_status'])
            ->removeColumn('id')
            ->make(true);
    }
}
