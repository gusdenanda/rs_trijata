<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\HasilSwab;
use App\Models\SubkategoriPasien;
use App\Models\JenisSwab;
use App\Models\Setting;
use App\Models\PenandatanganSwab;

use Yajra\DataTables\Facades\DataTables;


use App\Imports\HasilSwabImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use PDF;
use QrCode;

class HasilSwabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subkategori_pasien = SubkategoriPasien::select('id', 'skp_nama')
                            ->where('skp_active', '1')
                            ->get();
        $jenis_swab = JenisSwab::select('id', 'js_nama')
                            ->where('js_active', '1')
                            ->get();
        $dr_pj = PenandatanganSwab::where('ps_penandatangan','1')
                            ->where('ps_active','1')->first();

        return view('hasil_swab',compact('subkategori_pasien','jenis_swab','dr_pj'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = Hash::make(($request->hs_nik.date("U")));
        $code = str_replace("/","-",$code);
        $hs_usia = $years = Carbon::parse($request->hs_tgl_lahir)->age;
        $request->hs_ct_value = str_replace(",",".", $request->hs_ct_value);
        if($request->hs_ct_value==""){
            $request->hs_ct_value=NULL;
        }
        if($request->js_id=="1"){
            HasilSwab::create([
                'hs_nama'      => $request->hs_nama,
                'hs_nik'     => $request->hs_nik,
                'hs_tgl_lahir'     => $request->hs_tgl_lahir,
                'hs_usia'     => $hs_usia,
                'hs_jk'     => $request->hs_jk,
                'hs_no_hp'     => $request->hs_no_hp,
                'hs_alamat'     => $request->hs_alamat,
                'hs_faskes'     => $request->hs_faskes,
                'hs_sampel_ke'     => $request->hs_sampel_ke,
                'js_id'     => $request->js_id,
                'hs_nomor_spesimen'     => $request->hs_nomor_spesimen,
                'hs_tgl_kirim_spesimen'     => $request->hs_tgl_kirim_spesimen,
                'hs_tgl_terima_spesimen'     => $request->hs_tgl_terima_spesimen,
                'hs_tgl_periksa_spesimen'     => $request->hs_tgl_periksa_spesimen,
                'hs_hasil'     => $request->hs_hasil,
                'hs_kriteria'     => $request->hs_kriteria,
                'hs_satker'     => $request->hs_satker,
                'skp_id'     => $request->skp_id,
                'hs_ct_value'     => $request->hs_ct_value,
                'hs_jam_kirim_spesimen' => $request->hs_jam_kirim_spesimen,
                'hs_jam_terima_spesimen' => $request->hs_jam_terima_spesimen,
                'hs_jam_periksa_spesimen' => $request->hs_jam_periksa_spesimen,
                'hs_kode_lab' => $request->hs_kode_lab,
                'hs_diperiksa_oleh' => $request->hs_diperiksa_oleh,
                'hs_diverifikasi_oleh' => $request->hs_diverifikasi_oleh,
                'hs_code' => $code
            ]);
        }else{
            HasilSwab::create([
                'hs_nama'      => $request->hs_nama,
                'hs_nik'     => $request->hs_nik,
                'hs_tgl_lahir'     => $request->hs_tgl_lahir,
                'hs_usia'     => $hs_usia,
                'hs_jk'     => $request->hs_jk,
                'hs_no_hp'     => $request->hs_no_hp,
                'hs_alamat'     => $request->hs_alamat,
                'hs_faskes'     => $request->hs_faskes,
                'hs_sampel_ke'     => $request->hs_sampel_ke,
                'js_id'     => $request->js_id,
                'hs_nomor_spesimen_antigen'     => $request->hs_nomor_spesimen,
                'hs_tgl_kirim_spesimen'     => $request->hs_tgl_kirim_spesimen,
                'hs_tgl_terima_spesimen'     => $request->hs_tgl_terima_spesimen,
                'hs_tgl_periksa_spesimen'     => $request->hs_tgl_periksa_spesimen,
                'hs_hasil'     => $request->hs_hasil,
                'hs_kriteria'     => $request->hs_kriteria,
                'hs_satker'     => $request->hs_satker,
                'skp_id'     => $request->skp_id,
                'hs_ct_value'     => $request->hs_ct_value,
                'hs_jam_kirim_spesimen' => $request->hs_jam_kirim_spesimen,
                'hs_jam_terima_spesimen' => $request->hs_jam_terima_spesimen,
                'hs_jam_periksa_spesimen' => $request->hs_jam_periksa_spesimen,
                'hs_kode_lab' => $request->hs_kode_lab,
                'hs_diperiksa_oleh' => $request->hs_diperiksa_oleh,
                'hs_diverifikasi_oleh' => $request->hs_diverifikasi_oleh,
                'hs_code' => $code
            ]);
        }
        return $this->status(1, 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = HasilSwab::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hs_usia = $years = Carbon::parse($request->hs_tgl_lahir)->age;
        $request->hs_ct_value = str_replace(",",".", $request->hs_ct_value);
        if($request->hs_ct_value==""){
            $request->hs_ct_value=NULL;
        }
        if($request->js_id=="1"){
            HasilSwab::whereId($id)->update([
                'hs_nama'      => $request->hs_nama,
                'hs_nik'     => $request->hs_nik,
                'hs_tgl_lahir'     => $request->hs_tgl_lahir,
                'hs_usia'     => $hs_usia,
                'hs_jk'     => $request->hs_jk,
                'hs_no_hp'     => $request->hs_no_hp,
                'hs_alamat'     => $request->hs_alamat,
                'hs_faskes'     => $request->hs_faskes,
                'hs_sampel_ke'     => $request->hs_sampel_ke,
                'js_id'     => $request->js_id,
                'hs_nomor_spesimen'     => $request->hs_nomor_spesimen,
                'hs_tgl_kirim_spesimen'     => $request->hs_tgl_kirim_spesimen,
                'hs_tgl_terima_spesimen'     => $request->hs_tgl_terima_spesimen,
                'hs_tgl_periksa_spesimen'     => $request->hs_tgl_periksa_spesimen,
                'hs_hasil'     => $request->hs_hasil,
                'hs_kriteria'     => $request->hs_kriteria,
                'hs_satker'     => $request->hs_satker,
                'hs_ct_value'     => $request->hs_ct_value,
                'skp_id'     => $request->skp_id,
                'hs_jam_kirim_spesimen' => $request->hs_jam_kirim_spesimen,
                'hs_jam_terima_spesimen' => $request->hs_jam_terima_spesimen,
                'hs_jam_periksa_spesimen' => $request->hs_jam_periksa_spesimen,
                'hs_kode_lab' => $request->hs_kode_lab,
                'hs_diperiksa_oleh' => $request->hs_diperiksa_oleh,
                'hs_diverifikasi_oleh' => $request->hs_diverifikasi_oleh
            ]); 
        }else{
            if($request->hs_tgl_periksa_spesimen!=$request->hs_tgl_periksa_spesimen_old){
                //generate ulang nomor spesimen
                $data = HasilSwab::where('js_id','=','2')
                                    ->whereMonth('hs_tgl_periksa_spesimen', '=',date('m'))
                                    ->whereYear('hs_tgl_periksa_spesimen', '=', date('Y'))
                                    ->max('hs_nomor_spesimen_antigen');
                if ( is_null($data) ) {
                    $data=1;
                }else{
                    if($data==""){
                        $data=1;
                    }else{
                        $data= (int)$data + 1;
                    }
                     
                }
                $request->hs_nomor_spesimen = $data;
            }
            HasilSwab::whereId($id)->update([
                'hs_nama'      => $request->hs_nama,
                'hs_nik'     => $request->hs_nik,
                'hs_tgl_lahir'     => $request->hs_tgl_lahir,
                'hs_usia'     => $hs_usia,
                'hs_jk'     => $request->hs_jk,
                'hs_no_hp'     => $request->hs_no_hp,
                'hs_alamat'     => $request->hs_alamat,
                'hs_faskes'     => $request->hs_faskes,
                'hs_sampel_ke'     => $request->hs_sampel_ke,
                'js_id'     => $request->js_id,
                'hs_nomor_spesimen_antigen'     => $request->hs_nomor_spesimen,
                'hs_tgl_kirim_spesimen'     => $request->hs_tgl_kirim_spesimen,
                'hs_tgl_terima_spesimen'     => $request->hs_tgl_terima_spesimen,
                'hs_tgl_periksa_spesimen'     => $request->hs_tgl_periksa_spesimen,
                'hs_hasil'     => $request->hs_hasil,
                'hs_kriteria'     => $request->hs_kriteria,
                'hs_satker'     => $request->hs_satker,
                'hs_ct_value'     => $request->hs_ct_value,
                'skp_id'     => $request->skp_id,
                'hs_jam_kirim_spesimen' => $request->hs_jam_kirim_spesimen,
                'hs_jam_terima_spesimen' => $request->hs_jam_terima_spesimen,
                'hs_jam_periksa_spesimen' => $request->hs_jam_periksa_spesimen,
                'hs_kode_lab' => $request->hs_kode_lab,
                'hs_diperiksa_oleh' => $request->hs_diperiksa_oleh,
                'hs_diverifikasi_oleh' => $request->hs_diverifikasi_oleh,
            ]); 
        }   
        
        return $this->status(1, 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        HasilSwab::where('id', $id)->delete();
        
        return $this->status(1, 'Data berhasil dihapus');
    }
    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $hasil_swab = HasilSwab::join('subkategori_pasien', 'hasil_swab.skp_id', 'subkategori_pasien.id')
                        ->select(['hasil_swab.id', 'hasil_swab.hs_nama','hasil_swab.hs_nik', 'hasil_swab.hs_tgl_lahir', 'hasil_swab.js_id', 'hasil_swab.hs_tgl_periksa_spesimen','hasil_swab.hs_usia','hasil_swab.hs_no_hp','hasil_swab.hs_alamat','hasil_swab.hs_faskes','hasil_swab.hs_hasil','hasil_swab.hs_satker','hasil_swab.hs_code','subkategori_pasien.skp_nama'])
                        ->orderBy('hasil_swab.hs_tgl_periksa_spesimen', 'DESC') ;
        
        return DataTables::of($hasil_swab)
            ->addColumn('aksi', function ($item) {
                /*return '<form action="hasil-swab/destroy/'. $item->id .'" class="text-center" method="POST">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success pdf" data-value="'. $item->hs_code .'" data-nama="'. $item->hs_nama .'"><a title="Cetak Hasil PDF Bahasa Indonesia" href="' . route('hasil-swab.generatepdf', $item->hs_code) .'" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9V2h12v7"/><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"/><path d="M6 14h12v8H6z"/></g></svg></a></button>
                                <button type="button" class="btn btn-primary pdf" data-value="'. $item->hs_code .'" data-nama="'. $item->hs_nama .'"><a title="Cetak Hasil PDF Bahasa Inggris" href="' . route('hasil-swab.generatepdf-en', $item->hs_code) .'" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9V2h12v7"/><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"/><path d="M6 14h12v8H6z"/></g></svg></a></button>
                                <button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->hs_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button> 
                                <button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
                            </div>
                        </form>';*/
                return '<form action="hasil-swab/destroy/'. $item->id .'" class="text-center" method="POST">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success pdf" data-value="'. $item->hs_code .'" data-nama="'. $item->hs_nama .'"><a title="Cetak Hasil PDF Bahasa Indonesia" href="' . route('hasil-swab.generatepdf', $item->hs_code) .'" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" stroke-linecap="round" stroke-linejoin="round"><path d="M6 9V2h12v7"/><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"/><path d="M6 14h12v8H6z"/></g></svg></a></button>
                            <button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->hs_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button> 
                            <button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
                        </div>
                    </form>';
            })
            ->addColumn('checkbox', function ($item) {
                return '<input style="text-align:center;" type="checkbox" name="hs_id[]" value="'.$item->hs_code.'"/>';
            })
            ->addColumn('js_id', function ($item) {
                if($item->js_id=="2"){
                    $st = '<div disabled class="badge badge-md badge-warning">Swab Antigen</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-danger">Swab PCR</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','checkbox','js_id'])
            ->removeColumn('id')
            ->make(true);
    }

    public function generateNomor() 
    {
        $data = HasilSwab::where('js_id','=','2')
                            ->whereMonth('hs_tgl_periksa_spesimen', '=',date('m'))
                            ->whereYear('hs_tgl_periksa_spesimen', '=', date('Y'))
                            ->max('hs_nomor_spesimen_antigen');
        //return $data;
        if ( is_null($data) ) {
            $data=1;
        }else{
            if($data==""){
                $data=1;
            }else{
                $data= (int)$data + 1;
            }
            
        }
        return json_encode($data);

    }

    public function import_excel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
        
        // menangkap file excel
        $file = $request->file('file');
        
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
        
       // upload ke folder file_excel di dalam folder public
       $file->move('repositories/public/file_excel',$nama_file);
        
       // import data
       //Excel::import(new HasilSwabImport, public_path('repositories/public/file_excel/'.$nama_file));
       Excel::import(new HasilSwabImport, public_path('file_excel/'.$nama_file));
        
        return $this->status(1, 'File Berhasil di Import');

    }
    
    public function generatePDF($id)
    {
        $data = HasilSwab::where('hs_code', $id)->first();
        $dr_pj = PenandatanganSwab::where('ps_penandatangan','1')
                        ->where('ps_active','1')->first();
        $data["ps_nama"] = $dr_pj->ps_nama;
        $data["ps_nrp"] = $dr_pj->ps_nrp;
        $data["ps_ttd"] = $dr_pj->ps_ttd;
        $data["ps_pangkat"] = $dr_pj->ps_pangkat;

        $pemeriksa = PenandatanganSwab::where('ps_penandatangan','2')
                        ->where('ps_active','1')->first();
        $data["pemeriksa_nama"] = $pemeriksa->ps_nama;
        $data["pemeriksa_nrp"] = $pemeriksa->ps_nrp;
        $data["pemeriksa_pangkat"] = $pemeriksa->ps_pangkat;
        $data["pemeriksa_ttd"] = $pemeriksa->ps_ttd;

        $setting = Setting::first();
        $data["ct_value"] = $setting->ct_value;
        
        $qrcode = base64_encode(QrCode::format('svg')->size(70)->errorCorrection('H')->generate("".request()->getHttpHost()."/cek-validation-swab/".$data->hs_code.""));
        $data["qrcode"] = $qrcode;
        $config = ['instanceConfigurator' => function($pdf) {
            $pdf->SetWatermarkText('RS BHAYANGKARA DENPASAR',0.1);
            $pdf->showWatermarkText = true;
        }];
        if($data->js_id=="2"){
            $pdf = PDF::loadView('hasilPDFSwabAntigen', $data, [], $config);
        }else{
            $pdf = PDF::loadView('hasilPDFSwab', $data, [], $config);
        }
        $pdf->SetProtection(['print'], '', 'T1dakB0l3hC0py2021');
        return $pdf->stream('hasil_swab'. $data["id"].'.pdf');
    }

    public function generatePDFEn($id)
    {
        $data = HasilSwab::where('hs_code', $id)->first();
        $dr_pj = PenandatanganSwab::where('ps_penandatangan','1')
                        ->where('ps_active','1')->first();
        $data["ps_nama"] = $dr_pj->ps_nama;
        $data["ps_nrp"] = $dr_pj->ps_nrp;
        $data["ps_ttd"] = $dr_pj->ps_ttd;
        $data["ps_pangkat"] = $dr_pj->ps_pangkat;

        $pemeriksa = PenandatanganSwab::where('ps_penandatangan','2')
                        ->where('ps_active','1')->first();
        $data["pemeriksa_nama"] = $pemeriksa->ps_nama;
        $data["pemeriksa_nrp"] = $pemeriksa->ps_nrp;
        $data["pemeriksa_pangkat"] = $pemeriksa->ps_pangkat;
        $data["pemeriksa_ttd"] = $pemeriksa->ps_ttd;

        $setting = Setting::first();
        $data["ct_value"] = $setting->ct_value;

        $qrcode = base64_encode(QrCode::format('svg')->size(70)->errorCorrection('H')->generate("".request()->getHttpHost()."/cek-validation-swab/en/".$data->hs_code.""));
        $data["qrcode"] = $qrcode;
        $config = ['instanceConfigurator' => function($pdf) {
            $pdf->SetWatermarkText('RS BHAYANGKARA DENPASAR');
            $pdf->showWatermarkText = true;
        }];
        
        $pdf = PDF::loadView('hasilPDFEn', $data, [], $config);
        $pdf->SetProtection(['print'], '', 'T1dakB0l3hC0py2021');
        return $pdf->stream('swab_test'. $data["id"].'.pdf');
    }

    public function generatePDFMultiple(Request $request)
    {
        $data = array();
        $i = 0;
        $setting = Setting::first();
        foreach($request->hs_id as $hs_id){
            $data[$i] = HasilSwab::where('hs_code', $hs_id)->first();
            $dr_pj = PenandatanganSwab::where('ps_penandatangan','1')
                            ->where('ps_active','1')->first();
            $data[$i]["ps_nama"] = $dr_pj->ps_nama;
            $data[$i]["ps_nrp"] = $dr_pj->ps_nrp;
            $data[$i]["ps_ttd"] = $dr_pj->ps_ttd;
            $data[$i]["ps_pangkat"] = $dr_pj->ps_pangkat;

            $pemeriksa = PenandatanganSwab::where('ps_penandatangan','2')
                            ->where('ps_active','1')->first();
            $data[$i]["pemeriksa_nama"] = $pemeriksa->ps_nama;
            $data[$i]["pemeriksa_nrp"] = $pemeriksa->ps_nrp;
            $data[$i]["pemeriksa_pangkat"] = $pemeriksa->ps_pangkat;
            $data[$i]["pemeriksa_ttd"] = $pemeriksa->ps_ttd;

            $data[$i]["ct_value"] = $setting->ct_value;

            $qrcode = base64_encode(QrCode::format('svg')->size(70)->errorCorrection('H')->generate("".request()->getHttpHost()."/cek-validation-swab/".$data[$i]->hs_code.""));
            $data[$i]["qrcode"] = $qrcode;
            $i++;
        }
        
        $config = ['instanceConfigurator' => function($pdf) {
            $pdf->SetWatermarkText('RS BHAYANGKARA DENPASAR');
            $pdf->showWatermarkText = true;
        }];
        
        $pdf = PDF::loadView('hasilPDFMultiple', compact('data'), [], $config);
        $pdf->SetProtection(['print'], '', 'T1dakB0l3hC0py2021');
        return $pdf->stream('swab_test.pdf');
    }

    public function cekValidation($id)
    {
        $data = HasilSwab::where('hs_code', $id)->get();
        $count = $data->count();
        if ( $count==0 ) {
            $data1 = array();
            $data1["status"]=0;
            $data1["view"]="surat";
            $data1 = (object) $data1;
            
        }else{
            $data1 = array();
            $data1["status"]=1;
            $data1["view"]="surat";
            $data1 = (object) $data1;
        }
        return view('cek_validation_swab',compact('data','data1'));
    }
    public function cekValidationEn($id)
    {
        $data = HasilSwab::where('hs_code', $id)->get();
        $count = $data->count();
        if ( $count==0 ) {
            $data1 = array();
            $data1["status"]=0;
            $data1["view"]="surat";
            $data1 = (object) $data1;
        }else{
            $data1 = array();
            $data1["status"]=1;
            $data1["view"]="surat";
            $data1 = (object) $data1;
        }
        return view('cek_validation_swab_en',compact('data','data1'));
    }
    public function searchHasilSwab(Request $request)
    {
        $data = HasilSwab::where('hs_tgl_lahir', $request->tanggal_lahir)
                            ->where('hs_tgl_periksa_spesimen', $request->tanggal_periksa)
                            ->get();
        //return $data;
        $count = $data->count();
        if ( $count==0 ) {
            $data1 = array();
            $data1["status"]=0;
            $data1["view"]="web";
            $data1 = (object) $data1;
        }else{
            $data1 = array();
            $data1["status"]=1;
            $data1["view"]="web";
            $data1 = (object) $data1;
        }
        return view('cek_validation_swab',compact('data','data1'));
    }
    public function searchHasilSwabEn(Request $request)
    {
        $data = HasilSwab::where('hs_tgl_lahir', $request->tanggal_lahir)
                            ->where('hs_tgl_periksa_spesimen', $request->tanggal_periksa)
                            ->get();
        $count = $data->count();
        if ( $count==0 ) {
            $data1 = array();
            $data1["status"]=0;
            $data1["view"]="web";
            $data1 = (object) $data1;
        }else{
            $data1 = array();
            $data1["status"]=1;
            $data1["view"]="web";
            $data1 = (object) $data1;
        }
        return view('cek_validation_swab_en',compact('data','data1'));
    }
    

    public function viewReport(Request $request)
    {
        $subkategori_pasien_master = SubkategoriPasien::select('id', 'skp_nama')
                            ->where('skp_active', '1')
                            ->get();
        $js_id = JenisSwab::select('id', 'js_nama')
                            ->where('js_active', '1')
                            ->get();
        $datenow = date("Y-m-d");
        
        if($request->from_date!=""){
            $hasil_swab = HasilSwab::select(
                            'hasil_swab.id', 
                            'hasil_swab.hs_jk', 
                            'hasil_swab.hs_nama',
                            'hasil_swab.hs_nik', 
                            'hasil_swab.hs_tgl_lahir', 
                            'hasil_swab.hs_tgl_kirim_spesimen',
                            'hasil_swab.hs_tgl_terima_spesimen',
                            'hasil_swab.hs_tgl_periksa_spesimen',
                            'hasil_swab.hs_usia',
                            'hasil_swab.hs_no_hp',
                            'hasil_swab.hs_alamat',
                            'hasil_swab.hs_faskes',
                            'hasil_swab.hs_hasil',
                            'hasil_swab.hs_satker',
                            'hasil_swab.hs_code',
                            'hasil_swab.js_id',
                            'subkategori_pasien.skp_nama'
                        )
                        ->leftJoin("subkategori_pasien", "subkategori_pasien.id", "=", "hasil_swab.skp_id");
            $hasil_swab->whereBetween('hs_tgl_periksa_spesimen', [$request->from_date,$request->to_date]);
            
        }else{
            $hasil_swab = HasilSwab::select(
                            'hasil_swab.id', 
                            'hasil_swab.hs_jk', 
                            'hasil_swab.hs_nama',
                            'hasil_swab.hs_nik', 
                            'hasil_swab.hs_tgl_lahir', 
                            'hasil_swab.hs_tgl_kirim_spesimen',
                            'hasil_swab.hs_tgl_terima_spesimen',
                            'hasil_swab.hs_tgl_periksa_spesimen',
                            'hasil_swab.hs_usia',
                            'hasil_swab.hs_no_hp',
                            'hasil_swab.hs_alamat',
                            'hasil_swab.hs_faskes',
                            'hasil_swab.hs_hasil',
                            'hasil_swab.hs_satker',
                            'hasil_swab.hs_code',
                            'hasil_swab.js_id',
                            'subkategori_pasien.skp_nama'
                        )
                        ->leftJoin("subkategori_pasien", "subkategori_pasien.id", "=", "hasil_swab.skp_id")
                        ->whereBetween('hasil_swab.hs_tgl_periksa_spesimen', [$datenow,$datenow]);     
                
        }
        if($request->js_id!="")
        {
            $hasil_swab->where('js_id', $request->js_id);
        }
        if($request->skp_id!="")
        {
            $hasil_swab->where('skp_id', $request->skp_id);
        }
        $hasil_swab = $hasil_swab->get(); 
        
        $data = array();
        $data["keyword"]=$request->keyword;
        $data["from_date"]=$request->from_date;
        $data["to_date"]=$request->to_date;
        $data["skp_id"]=$request->skp_id;
        $data["js_id"]=$request->js_id;
        $data = (object) $data;
        //return $hasil_swab;
        return view('report_hasil_swab',compact('subkategori_pasien_master','hasil_swab','data','js_id'));
    }

    public function viewDataReport()
    {
        $hasil_swab = HasilSwab::select(['id', 'hs_nama','hs_nik', 'hs_usia','hs_no_hp','hs_alamat','hs_faskes','hs_hasil','hs_satker','hs_tgl_lahir','hs_jenis_spesimen','']);
        
        return DataTables::of($hasil_swab)
            ->removeColumn('id')
            ->make(true);
    }
    public function cetakPDF(Request $request)
    {
        $subkategori_pasien = SubkategoriPasien::select('id', 'skp_nama')
                            ->where('skp_active', '1')
                            ->get();
        $datenow = date("Y-m-d");
        if($request->from_date!=""){
            $hasil_swab = HasilSwab::select(
                            'hasil_swab.id', 
                            'hasil_swab.hs_jk', 
                            'hasil_swab.hs_nama',
                            'hasil_swab.hs_nik', 
                            'hasil_swab.hs_tgl_lahir', 
                            'hasil_swab.hs_tgl_kirim_spesimen',
                            'hasil_swab.hs_tgl_terima_spesimen',
                            'hasil_swab.hs_tgl_periksa_spesimen',
                            'hasil_swab.hs_usia',
                            'hasil_swab.hs_no_hp',
                            'hasil_swab.hs_alamat',
                            'hasil_swab.hs_faskes',
                            'hasil_swab.hs_hasil',
                            'hasil_swab.hs_satker',
                            'hasil_swab.hs_code',
                            'hasil_swab.js_id',
                            'subkategori_pasien.skp_nama'
                        )
                        ->leftJoin("subkategori_pasien", "subkategori_pasien.id", "=", "hasil_swab.skp_id");
            $hasil_swab->whereBetween('hs_tgl_periksa_spesimen', [$request->from_date,$request->to_date]);
        }else{
            $hasil_swab = HasilSwab::select(
                            'hasil_swab.id', 
                            'hasil_swab.hs_jk', 
                            'hasil_swab.hs_nama',
                            'hasil_swab.hs_nik', 
                            'hasil_swab.hs_tgl_lahir', 
                            'hasil_swab.hs_tgl_kirim_spesimen',
                            'hasil_swab.hs_tgl_terima_spesimen',
                            'hasil_swab.hs_tgl_periksa_spesimen',
                            'hasil_swab.hs_usia',
                            'hasil_swab.hs_no_hp',
                            'hasil_swab.hs_alamat',
                            'hasil_swab.hs_faskes',
                            'hasil_swab.hs_hasil',
                            'hasil_swab.hs_satker',
                            'hasil_swab.hs_code',
                            'hasil_swab.js_id',
                            'subkategori_pasien.skp_nama'
                        )
                        ->leftJoin("subkategori_pasien", "subkategori_pasien.id", "=", "hasil_swab.skp_id")
                        ->whereBetween('hasil_swab.hs_tgl_periksa_spesimen', [$datenow,$datenow]);    
                
        }
        if($request->js_id!="")
        {
            $hasil_swab->where('js_id', $request->js_id);
        }
        if($request->skp_id!="")
        {
            $hasil_swab->where('skp_id', $request->skp_id);
        }
        $hasil_swab = $hasil_swab->get(); 
        $dr_pj = PenandatanganSwab::where('ps_penandatangan','3')
                        ->where('ps_active','1')->first();
        //$pdf = PDF::loadView('cetakPDF', compact('hasil_swab','dr_pj'))->setPaper('legal', 'landscape');
        $pdf = PDF::loadView('cetakPDF', compact('hasil_swab','dr_pj'), [], ['format' => 'Legal-L', 'mirrorMargins' => 1]);
    
        return $pdf->stream('report_swab.pdf');
    }
}
