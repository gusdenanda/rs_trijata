<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\HasilSwab;
use App\Models\SubkategoriPasien;
use App\Models\User;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $total_swab = HasilSwab::count(); 
        $total_swab_negatif = HasilSwab::where('hs_hasil','negatif')->count(); 
        $total_swab_positif = HasilSwab::where('hs_hasil','positif')->count(); 
        $persentase_positif = round(($total_swab_positif/$total_swab),2) * 100;
        $persentase_negatif = round(($total_swab_negatif/$total_swab),2) * 100;
        $total_user = User::count(); 

        //$total_kategori_pasien = HasilSwab::groupBy('skp_id')->get();
        $total_kategori_pasien = HasilSwab::select(DB::raw('subkategori_pasien.skp_nama, COUNT(*) AS count, hasil_swab.hs_hasil'))
                    ->join('subkategori_pasien', 'subkategori_pasien.id', '=', 'hasil_swab.skp_id')
                    ->whereIn('hasil_swab.hs_hasil', array('positif','negatif'))
                    ->groupBy('subkategori_pasien.skp_nama')
                    ->groupBy('hasil_swab.hs_hasil')
                    ->orderBy('subkategori_pasien.skp_nama', 'ASC')->get();

        $users = HasilSwab::select('id', 'created_at')->where('hs_hasil','positif')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m');
            });

        $usermcount = [];
        $blnpos = [];

        foreach ($users as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        $month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        for ($i = 1; $i <= 12; $i++) {
            if (!empty($usermcount[$i])) {
                $blnpos[$i]['count'] = $usermcount[$i];
            } else {
                $blnpos[$i]['count'] = 0;
            }
            $blnpos[$i]['month'] = $month[$i - 1];
        }

        $users = HasilSwab::select('id', 'created_at')->where('hs_hasil','negatif')
            ->get()
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m');
            });

        $usermcount = [];
        $blnneg = [];

        foreach ($users as $key => $value) {
            $usermcount[(int)$key] = count($value);
        }

        for ($i = 1; $i <= 12; $i++) {
            if (!empty($usermcount[$i])) {
                $blnneg[$i]['count'] = $usermcount[$i]*-1;
            } else {
                $blnneg[$i]['count'] = 0;
            }
            $blnneg[$i]['month'] = $month[$i - 1];
        }
        
        $array_icon = array("bg-light-primary","bg-light-warning","bg-light-success","bg-light-primary","bg-light-warning","bg-light-success","bg-light-primary","bg-light-warning");
        return view('dashboard',compact('total_swab','array_icon','total_swab_negatif','total_swab_positif','total_user','total_kategori_pasien','blnpos','blnneg','persentase_positif','persentase_negatif'));
    }
    public function reportPersubkategori($id)
    {
        $data = HasilSwab::select(DB::raw('hasil_swab.hs_satker, COUNT(*) AS count, hasil_swab.hs_hasil'))
                    ->join('subkategori_pasien', 'subkategori_pasien.id', '=', 'hasil_swab.skp_id')
                    ->where('subkategori_pasien.skp_nama',$id)
                    ->groupBy('hasil_swab.hs_satker')
                    ->groupBy('hasil_swab.hs_hasil')->get();
        return json_encode($data);
    }
}
