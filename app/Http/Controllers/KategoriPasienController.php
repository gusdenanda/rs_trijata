<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\KategoriPasien;

use Yajra\DataTables\Facades\DataTables;

class KategoriPasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kategori_pasien');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        KategoriPasien::storeKategoriPasien($request);
        
        return $this->status(1, 'Kategori Pasien berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KategoriPasien::where('id', $id)->first();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        KategoriPasien::where('id', $id)->update([
            'kp_nama' => $request->kp_nama,
            'kp_active'  => $request->kp_active
        ]);
        
        return $this->status(1, 'Kategori Pasien berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        KategoriPasien::where('id', $id)->delete();
        
        return $this->status(1, 'Kategori Pasien berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
    public function data()
    {
        $kategori_pasien = KategoriPasien::select(['id', 'kp_nama', 'kp_active']);
        
        return DataTables::of($kategori_pasien)
            ->addColumn('aksi', function ($item) {
                return '<form action="kategori-pasien/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->kp_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->addColumn('kp_status', function ($item) {
                if($item->kp_active=="1"){
                    $st = '<div disabled class="badge badge-md badge-success">Aktif</div>';
                }else{
                    $st = '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                }
                return $st;
            })
            ->rawColumns(['aksi','kp_status'])
            ->removeColumn('kp_id')
            ->make(true);
    }
}
